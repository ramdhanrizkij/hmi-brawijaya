<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\KategoriBankData;

class BankData extends Model
{
    use HasFactory;

    public function kategori(Type $var = null)
    {
        return $this->belongsTo(KategoriBankData::class,'kategori_id','id');
    }
}
