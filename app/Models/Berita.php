<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    use HasFactory;

    protected $table = "berita";
    protected $fillable = ["judul","slug","content","thumbnail","user_id",
                            "published_by","meta_title",
                            "meta_description","meta_keywords",
                            "views_count","status","kategori_id"];

    public function kategori()
    {
        return $this->belongsTo('App\Models\Kategori','kategori_id','id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }
}
