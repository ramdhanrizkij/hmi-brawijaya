<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Card extends Component
{
    public $withFooter;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($withFooter)
    {
        //
        $this->withFooter = $withFooter ?? false;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.card');
    }
}
