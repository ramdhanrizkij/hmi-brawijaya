<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //

        Gate::define('isAdministrator', function ($user) {
           return $user->role->role_name == "Administrator";
        });

        Gate::define('isEditor', function ($user) {
            return $user->role->role_name == "Editor";
        });

        Gate::define('isContributor', function ($user) {
            return $user->role->role_name == "Contributor";
        });

    }
}
