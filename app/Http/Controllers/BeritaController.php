<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Berita;
use App\Models\Kategori;
use App\Models\Tag;
use Illuminate\Support\Str;
use Yajra\Datatables\Datatables;
use Auth;
use DB;

class BeritaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('admin.berita.list');
    }

    public function create(Request $request)
    {
        $tags = Tag::orderBy('tag','asc')->get();
        $kategori = Kategori::orderBy('nama_kategori','asc');
        if($request->user()->role_id==2) {
            $kategori = $kategori->where('id',1);
        }
        $kategori = $kategori->get();
        return view('admin.berita.create',compact('kategori','tags'));
    }

    public function store(Request $request)
    {
        $status = 0;
        if($request->get('submit',null)) {
            $status = 1;
        }
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori' => 'required',
            'tags' => 'required',
            'thumbnail' => 'required|image',
        ]);
        $tags = $request->tags;
        DB::beginTransaction();
        try {
            $featured = $request->file('thumbnail');
            $featuerd_new = time().$featured->getClientoriginalName();
            $featured->move('uploads/berita', $featuerd_new);
            $berita = Berita::create([
                'judul' => $request->judul,
                'content' => $request->content,
                'slug' => str::slug($request->judul),
                'thumbnail' => $featuerd_new,
                'user_id' => $request->user()->id,
                'kategori_id' => $request->kategori,
                'meta_title' => $request->meta_title,
                'meta_description' => $request->meta_description,
                'meta_keywords' => $request->meta_keywords,
                'status' => $status,
            ]);
            $tagNames = [];
            if (!empty($tags)) {
                foreach ($tags as $item)
                {
                    $tag = Tag::where('tag',$item)->first();
                    if(!$tag){
                        $tag = new Tag;
                    }
                    $tag->tag = $item;
                    $tag->slug = Str::slug($item);
                    $tag->save();
                    // Tag::firstOrCreate(['tag'=>$item, 'slug'=>Str::slug($item)]);
                    if($tag) {
                        $tagNames[] = $tag->id;
                    }
                }
                $berita->tags()->syncWithoutDetaching($tagNames);
            }

            DB::commit();
            return redirect('/admin/berita')->with('success','Berhasil menambahkan berita');
        }catch(\Exception $ex) {
            DB::rollback();
            return redirect()->back()->withInput()->with('error','Gagal menambahkan berita, terjadi kesalahan dengan server');
        }
    }

    public function table(Request $request)
    {
        $data = Berita::with('tags')->with('kategori')->with('user');
        if(Auth::user()->role_id!=2){
            $user = Auth::user();
            $data = $data->where(function($query)use($user){
                $query->where('status','>',0)
                    ->orWhere('user_id','=',$user->id);
            });
        }else {
            $data = $data->where('user_id',Auth::user()->id);
        }

        $data = $data->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $result = '<div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Action
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="'.url('admin/berita/update/'.$row['id']).'" data-id="'.$row['id'].'">Update</a>
                  <a class="dropdown-item" href="'.url('admin/berita/delete/'.$row['id']).'" data-id="'.$row['id'].'" onclick="return confirm(`Apakah anda yakin akan menghapus data tersebut ?`);">Delete</a>';

                if(Auth::user()->role_id!=2) {
                    if($row['status']!=2){
                        $result .='<a class="dropdown-item" href="'.url('admin/berita/'.$row['id']).'/publish">Publish</a>';
                    }else {
                        $result .='<a class="dropdown-item" href="'.url('admin/berita/'.$row['id']).'/unpublish">Unpublish</a>';
                    }
                }
                $result .= '</div></div>';
                return $result;

            })
            ->addColumn('checkbox', function($row){
                return '<input type="checkbox" name="selected_checkbox" data-id="'.$row['id'].'"><label></label>';
            })
            ->addColumn('tags', function($row){
                $result = "";
                foreach($row["tags"] as $item) {
                    $result .= "&nbsp;<span class='badge badge-info'>$item->tag</span>";
                }
                return $result;
            })
            ->addColumn('created_at', function($row){
                return $row['created_at']->format('d M Y H:i');
            })
            ->addColumn('status', function($row){
                return $row['status']==0?'Draft':($row['status']==1?'Unpublish':'Publish');
            })
            ->rawColumns(['action','checkbox','tags','created_at','status'])
            ->make(true);
    }


    public function update(Request $request, $id)
    {
        $berita = Berita::with(['kategori','tags'])->findOrFail($id);
        $selected = [];
        foreach($berita->tags as $tag) {
            $selected[] = $tag->tag;
        }
        $berita->selected_tag = $selected;

        return view('admin.berita.update')->with([
            'kategori'=>Kategori::all(),
            'tags'=>Tag::all(),
            'berita'=>$berita
        ]);
    }

    public function saveUpdate(Request $request,$id)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori' => 'required',
            'tags' => 'required',
        ]);
        $tags = $request->tags;

        DB::beginTransaction();
        try {
            $post = Berita::findOrFail($id);
            if($request->hasfile('thumbnail')){
                $featured = $request->file('thumbnail');
                $featuerd_new = time().$featured->getClientoriginalName();
                $featured->move('uploads/berita', $featuerd_new);
                $post->thumbnail = $featuerd_new;
            }

            $post->judul = $request->judul;
            $post->content = $request->content;
            $post->slug = str::slug($request->judul);
            $post->user_id = $request->user()->id;
            $post->kategori_id = $request->kategori;
            $post->meta_title = $request->meta_title;
            $post->meta_description = $request->meta_description;
            $post->meta_keywords = $request->meta_keywords;
            $post->status = $request->draft?0:$post->status;

            $tagNames = [];
            if (!empty($tags)) {
                foreach ($tags as $item)
                {
                    $tag = Tag::where('tag',$item)->first();
                    if(!$tag){
                        $tag = new Tag;
                    }
                    $tag->tag = $item;
                    $tag->slug = Str::slug($item);
                    $tag->save();
                    // Tag::firstOrCreate(['tag'=>$item, 'slug'=>Str::slug($item)]);
                    if($tag) {
                        $tagNames[] = $tag->id;
                    }
                }
                $post->tags()->sync($tagNames);
            }
            $post->save();
            DB::commit();
            return redirect('/admin/berita')->with('success','Berhasil memperbaharui berita');
        }catch(\Exception $ex) {
            DB::rollback();
            dd($ex);
            return redirect()->back()->withInput()->with('error','Gagal memperbaharui berita, terjadi kesalahan dengan server');
        }


        $post->judul = $request->title;
        $post->content = $request->body;
        $post->slug = Str::slug($request->title);
        $post->kategori_id = $request->category_id;
        $post->save();
        $post->tags()->sync($request->tags);
        Session::flash('success','Berhasil melakukan update post');
        return redirect()->route('post.list')->withErrors($validate)->withInput();
    }

    public function delete(Request $request,$id)
    {
        $post = Berita::findOrFail($id);
        $post->delete();
        return redirect()->back()->with('success','Berhasil menghapus data berita');
    }

    public function bulkDelete(Request $request)
    {
        $post = Berita::whereIn('id',$request->post_ids)->delete();
        return response()->json([
            'status'=>200,
            'message'=>"Berhasil menghapus data berita",
            'data'=>[]
        ]);
    }

    // Change publish and un publish
    public function action(Request $request, $id, $aksi)
    {
        $action = 2;
        if($aksi!='publish'){
            $action = 1;
        }
        $berita = Berita::findOrFail($id);
        $berita->status = $action;
        if($action==1){
            $berita->published_by=null;
        }else{
            $berita->published_by = $request->user()->user_id;
        }
        $berita->save();
        $message = 'Berita berhasil di '.($action==2?'Publish':'Unpublish');
        return redirect()->back()->with('success',$message);
    }

    public function readNews(Request $request, $slug)
    {
        $berita = Berita::where('slug',$slug)->with('kategori','user','tags')->where('status',2)->first();
        $berita->views_count = $berita->views_count +1;
        $berita->save();
        $populer = Berita::orderBy('views_count','desc')->where('status',2)->where('berita.id','!=',$berita->id)->get();
        $next = Berita::where('status',2)->where('id','>',$berita->id)->first();
        $prev = Berita::where('status',2)->where('id','<',$berita->id)->first();
        $related = Berita::where('status',2)->where('kategori_id', $berita->kategori_id)->limit(8)->get();
        if(!$berita){
            abort(404);
        }
        return view('pages.news-detail',compact('berita','populer','next','prev','related'));
    }

    public function newsList(Request $request)
    {
        $berita = Berita::orderBy('created_at','desc')->paginate(9);
        return view('pages.news-list',compact('berita'));
    }

    public function newsKategori(Request $request,$slug)
    {
        $kategori = Kategori::where('slug',$slug)->first();
        if(!$kategori){
            abort(404);
        }
        $berita = Berita::where('kategori_id', $kategori->id)->orderBy('created_at','desc')->paginate(9);

        return view('pages.news-list',compact('berita','kategori'));
    }
}
