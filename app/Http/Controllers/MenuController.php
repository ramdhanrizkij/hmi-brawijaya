<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class MenuController extends Controller
{
    //
    public function index()
    {
        $this->authorize('isAdministrator');
        $menus = Menu::where('parent_id', '=', 0)->orderBy('posisi')->get();
        $allMenus = Menu::pluck('nama_menu', 'id')->all();

        return view('admin.menu.index', compact('menus', 'allMenus'));
    }

    public function tableAPI(Request $request)
    {
        $this->authorize('isAdministrator');

        $datas = Menu::latest()->get();
        return DataTables::of($datas)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                return '<div class="btn-group">
                              <button class="btn btn-sm btn-success" data-id="'.$row['id'].'" id="updateMenu"><em class="icon ni ni-edit"></em></button>
                              <button class="btn btn-sm btn-danger" data-id="'.$row['id'].'" id="deleteMenu"><em class="icon ni ni-trash"></em></button>
                        </div>';
            })
            ->addColumn('checkbox', function($row){
                return '<input type="checkbox" name="menu_checkbox" data-id="'.$row['id'].'"><label></label>';
            })
            ->rawColumns(['action','checkbox'])
            ->make(true);
    }

    public function save(Request $request)
    {
        $validate = $request->validate([
           'nama_menu' => 'required',
           'posisi' => 'required',
        ]);

        if(!$validate){
            return redirect()->back()->withErrors($validate)->withInput();
        }

        $parentId = $request->parent_id ?? 0;
        $link = $request->link_url ?? "#";

        $menu = Menu::updateOrCreate(
            ['id' => $request->id],
            [
                'nama_menu' => $request->nama_menu,
                'link_url' => $link,
                'posisi' => $request->posisi,
                'parent_id' => $parentId,
            ]
        );

        return response()->json([
            'status' => 200,
            'message' => 'Data menu berhasil disimpan',
            'data' => $menu
        ]);
    }
    public function findById($id)
    {
        $menu = Menu::findOrFail($id);
        return response()->json($menu);
    }

    public function destroy(Request $request)
    {
        $this->authorize('isAdministrator');

        $id = $request->id;
        $menu = Menu::findOrFail($id);
        $menu->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Berhasil menghapus menu',
            'data' => []
        ]);

    }

    public function deleteSelected(Request $request)
    {
        $this->authorize('isAdministrator');

        $menu = Menu::whereIn('id', $request->menu_ids)->delete();
        return response()->json([
            'status' => 200,
            'message' => 'Berhasil menghapus data menu',
            'data' => []
        ]);
    }

}
