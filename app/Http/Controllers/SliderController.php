<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SliderController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(! \Gate::any(['isAdministrator', 'isEditor'])){
            abort('403');
        }
        $slider = Slider::all();
        return view('admin.slider.list_banner')->with('datas', $slider);
    }

    public function save(Request $request)
    {
        if(! \Gate::any(['isAdministrator', 'isEditor'])){
            abort('403');
        }
        $validate = $request->validate([
            'judul' => 'required',
            'banner_img' => 'required_without:id',
        ]);

        if(!$validate){
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $id = $request->id ?? null;

        if($request->hasFile('banner_img'))
        {
            $image      = $request->file('banner_img');
            $fileName   = time().uniqid().".".$image->getClientOriginalExtension();
            $image->move('public/storage/banner', $fileName);
        }

        if($id != null)
        {
            $slider = Slider::find($id);
            if($request->hasFile('banner_img'))
            {
                if(file_exists(asset("public/storage/banner/".$slider->banner_img))){
                    unlink(asset("public/storage/banner/".$slider->banner_img));
                }
                $slider->banner_img = $fileName;
            }
            $slider->judul = $request->judul;
            $slider->link = $request->link;

            $slider->save();
        } else {
            $slider = Slider::create(
                [
                    'judul' => $request->judul,
                    'link' => $request->link ?? "#",
                    'banner_img' => $fileName,
                ]
            );
        }


        Session::flash('success', "Berhasil menyimpan data banner");
        return redirect()->route('slider.index');
    }

    public function destroy($id)
    {
        if(! \Gate::any(['isAdministrator', 'isEditor'])){
            abort('403');
        }
        $slider = Slider::findOrFail($id);

        if(file_exists($slider->banner_img))
        {
            unlink($slider->banner_img);
        }
        $slider->delete();
        Session::flash('success', "Berhasil menghapus data banner");
        return redirect()->route('slider.index');
    }
}
