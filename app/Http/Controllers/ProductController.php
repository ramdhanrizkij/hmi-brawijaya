<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\Product;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $kategori = ProductCategory::orderBy('kategori','asc')->get();
        $product = Product::orderBy('product_name','asc')->with('kategori')->get();
        return view('admin.product.index',compact('product','kategori'));
    }

    public function addProduct(Request $request)
    {
        $kategori = ProductCategory::orderBy('kategori','asc')->get();
        return view('admin.product.create',compact('kategori'));
    }

    public function storeProduct(Request $request)
    {
        $request->validate([
            'nama_produk'=>'required',
            'harga'=>'required',
            'kategori'=>'required',
            'gambar'=>'required'
        ]);

        $models = new Product;
        $models->product_name = $request->nama_produk;
        $models->slug = Str::slug($request->nama_produk, '-');
        $models->price = $request->harga;
        $models->description = $request->deskripsi;
        $models->category_id = $request->kategori;

        if($request->hasfile('gambar')){
            foreach($request->file('gambar') as $key=>$item) {
                $filename = time().$item->getClientoriginalName();
                $item->move('uploads/product', $filename);
                if($key==0) {
                    $models->thumbnail = $filename;
                    $models->save();
                }else {
                    $images = new ProductImage;
                    $images->product_id = $models->id;
                    $images->file = $filename;
                    $images->save();
                }
            }    
        }
        return redirect('/admin/product')->with('success','Product Berhasil Disimpan');
    }

    public function storeUpdate(Request $request, $id)
    {
        $request->validate([
            'nama_produk'=>'required',
            'deskripsi'=>'required',
            'harga'=>'required',
            'kategori'=>'required'
        ]);

        $models = Product::findOrFail($id);
        $models->product_name = $request->nama_produk;
        $models->slug = Str::slug($request->nama_produk, '-');
        $models->price = $request->harga;
        $models->description = $request->deskripsi;
        $models->category_id = $request->kategori;
        $models->save();
        if($request->hasfile('gambar')){
            foreach($request->file('gambar') as $key=>$item) {
                $filename = time().$item->getClientoriginalName();
                $item->move('uploads/product', $filename);
                $images = new ProductImage;
                $images->product_id = $models->id;
                $images->file = $filename;
                $images->save();
            }    
        }
        return redirect('/admin/product')->with('success','Product Berhasil Diubah');
    }

    public function deleteImage($id)
    {
        $models = ProductImage::findOrFail($id);
        @unlink(public_path('uploads/product/'.$models->file));
        $models->delete();
        return redirect()->back()->with('success','Gambar berhasil dihapus');
    }

    public function delete(Request $request, $id)
    {
        $models = Product::with('images')->findOrFail($id);
        $thumbnail = $models->thumbnail;
        @unlink(public_path('uploads/product/'.$thumbnail));
        foreach($models->images as $item) {
            @unlink(public_path('uploads/product/'.$item->file));
            ProductImage::find($item->id)->delete();
        }
        $models->delete();
        return redirect('/admin/product')->with('success','Berhasil Menghapus Data Product');
    }

    public function updateProduct(Request $request, $id)
    {
        $product = Product::with('kategori','images')->findOrFail($id);
        $kategori = ProductCategory::orderBy('kategori','asc')->get();
        return view('admin.product.update', compact('product','kategori'));
    }

    public function addKategori(Request $request)
    {
        $kategori = ProductCategory::all();
        return view('admin.product.kategori',compact('kategori'));
    }

    public function saveKategori(Request $request)
    {
        $kategori = $request->kategori;
        $slug = Str::slug($request->kategori, '-');
        $models = new ProductCategory;
        $models->kategori = $kategori;
        $models->slug = $slug;
        $models->save();
        return redirect()->back()->with('success','Berhasil menambahkan data kategori');
    }

    public function deleteKategori(Request $request,$id)
    {
        $models = ProductCategory::findOrFail($id);
        $models->delete();
        return redirect()->back()->with('success','Berhasil menghapuss data kategori');
    }

    public function updateKategori(Request $request)
    {
        $id =$request->get('id');
        $kategori = $request->get('kategori');
        $slug = Str::slug($request->kategori, '-');
        $models = ProductCategory::findOrFail($id);
        $models->kategori = $kategori;
        $models->slug = $slug;
        $models->save();
        return redirect()->back()->with('success','Berhasil mengubah data kategori');
    }
}
