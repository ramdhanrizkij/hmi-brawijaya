<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;


class PageController extends Controller
{
    private $kategori = ['syarat-dan-ketentuan','tentang-website'];

    public function read(Request $request, $slug)
    {
        if(!in_array($slug, $this->kategori)) {
            abort(404);
        }

        $page = Page::where('slug',$slug)->first();
        if(!$page){
            abort(404);
        }
        return view('pages.page_detail',compact('page'));
    }

    public function update(Request $request, $slug)
    {
        if(!in_array($slug, $this->kategori)) {
            abort(404);
        }

        $models = Page::where('slug',$slug)->first();
        if(!$models){
            $models = new Page;
            $models->slug = $slug;
            $models->judul = $slug;
            $models->content = "";
            $models->thumbnail = "noimage.png";
            $models->save();
        }

        return view('admin.page.update')->with('page',$models);
    }

    public function storeUpdate(Request $request,$id)
    {
        $models = Page::findOrFail($id);
        $request->validate([
            'judul'=>'required',
            'content'=>'required',
            'thumbnail'=>'required|image'
        ]);

        if($request->hasfile('thumbnail')){
            $featured = $request->file('thumbnail');
            $featuerd_new = time().$featured->getClientoriginalName();
            $featured->move('uploads/page', $featuerd_new);
            $models->thumbnail = $featuerd_new;
        }

        $models->judul = $request->judul;
        $models->content = $request->content;
        $models->meta_title = $request->meta_title;
        $models->meta_description = $request->meta_description;
        $models->meta_keywords = $request->meta_keywords;
        $models->save();

        return redirect()->back()->with('success','Berhasil memperbaharui data');
    }
}
