<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Berita;
use App\Models\Slider;
use App\Models\Video;
use Illuminate\Http\Request;

define('BERITA_ID',1);
define('LIGA_KOMPETISI',2);
define('CURAHAN_HATI',3);
define('TIPS_RUGBY',4);

class SearchController extends Controller
{
    //
    public function index(Request $request)
    {
        $populer = Berita::where('status',2)->orderBy('views_count','desc')->limit(8)->get();

        // Banner Iklan
        $homepage1 = Banner::where('kategori','homepage')->where('posisi',1)->first();
        $homepage2 = Banner::where('kategori','homepage')->where('posisi',2)->first();
        $homepage3 = Banner::where('kategori','homepage')->where('posisi',3)->first();
        $sidebar = Banner::where('kategori','sidebar')->where('posisi',1)->first();
        $bottom = Banner::where('kategori','bottom')->where('posisi',1)->first();

        $datas = Berita::with('tags')
            ->when($request->search, function ($query) use ($request){
                $query->where('judul', 'like', '%'.$request->search.'%');
            })
            ->orderBy('created_at', 'DESC')
            ->paginate(6)->withQueryString();

        if($datas->count() > 0){
            return view('search',compact('datas','populer',
                'homepage1','homepage2','homepage3','sidebar','bottom'));
        } else {
            return view('404');
        }

    }
}
