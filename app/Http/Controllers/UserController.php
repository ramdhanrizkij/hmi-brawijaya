<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $this->authorize('isAdministrator');

        $roles = Role::get();
        return view('admin.user.index')->with('roles', $roles);
    }

    public function tableAPI(Request $request)
    {
        $this->authorize('isAdministrator');

        $data = User::with('role')->where('id','<>', \Auth::user()->id )->latest()->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('photo_url', function ($row){
                return '<img src="'.asset('storage/user/'.$row['photo_url']).'" width="30px" />';
            })
            ->addColumn('action', function($row){
                return '<div class="btn-group">
                              <button class="btn btn-sm btn-success" data-id="'.$row['id'].'" id="updateUser">Update</button>
                              <button class="btn btn-sm btn-danger" data-id="'.$row['id'].'" id="deleteUser">Delete</button>
                        </div>';
            })
            ->addColumn('checkbox', function($row){
                return '<input type="checkbox" name="user_checkbox" data-id="'.$row['id'].'"><label></label>';
            })
            ->rawColumns(['photo_url','action','checkbox'])
            ->make(true);
    }


    public function save(Request $request)
    {
        $this->authorize('isAdministrator');


        $gravatarUrl = "https://www.gravatar.com/avatar/567460fe50742796467a826f1f2f2e43.jpg?s=200&d=mp";
        $id = $request->id ?? null;
        if($id != null){
            $validate = $request->validate([
                'name' => 'required',
                'email' => 'required|email',
                'role_id' => 'required',
            ]);
        } else {
            $validate = $request->validate([
                'name' => 'required',
                'email' => 'required',
                'role_id' => 'required',
                'photo_url' => 'required',
                'password' => 'required|confirmed',
            ]);
        }

        if(!$validate){
            return redirect()->back()->withErrors($validate)->withInput();
        }

        if($request->hasFile('photo_url'))
        {
            $image      = $request->file('photo_url');
            $fileName   = time().uniqid().".".$image->getClientOriginalExtension();
            $image->move('storage/user/', $fileName);
        }

        if($id != null)
        {
            $user = User::find($id);
            $oldFile = asset('storage/user/'.$fileName);
            if($request->hasFile('photo_url'))
            {
                if (file_exists($oldFile)){
                    unlink($oldFile);
                }
                $user->photo_url = $fileName;
            }
            $user->name = $request->name;
            $user->email = $request->email;
            $user->role_id = $request->role_id;
            $user->no_hp = $request->no_hp;
            $user->jabatan = $request->jabatan;
            $user->profile_desc = $request->profile_desc;

            $user->save();
        } else {
            $User = User::create(
                [
                    'name' => $request->name,
                    'email' => $request->email,
                    'photo_url' => $fileName ?? $gravatarUrl,
                    'role_id' => $request->role_id,
                    'no_hp' => $request->no_hp,
                    'jabatan' => $request->jabatan,
                    'profile_desc' => $request->profile_desc,
                    'password' => bcrypt($request->password),
                ]
            );
        }


        if($request->profile){
            Session::flash('success', "Berhasil menyimpan data user");
            return redirect()->route('profile');
        }
        Session::flash('success', "Berhasil menyimpan data user");
        return redirect()->route('user.index');
    }

    public function delete(Request $request)
    {
        $this->authorize('isAdministrator');

        $user = User::findOrFail($request->get('id'));
        $user->delete();
        return response()->json([
            'status'=>200,
            'message'=>'Berhasil menghapus data user',
            'result'=>[]
        ]);
    }

    public function getById($id)
    {
        $this->authorize('isAdministrator');

        $user = User::findOrFail($id);
        return response()->json($user);
    }

    public function profile()
    {
        $user = User::findOrFail(\Auth::user()->id);
        $roles = Role::get();

        return view('admin.user.profile')->with('user', $user)->with('roles', $roles);
    }

    public function changePwd(Request $request)
    {
        $validate = $request->validate([
            'password' => 'required|confirmed',
        ]);

        if(!$validate)
        {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        $user = User::find(\Auth::id());
        $user->password = bcrypt($request->password);
        $user->save();

        Session::flash('success', "Berhasil merubah password");
        return redirect()->route('profile');

    }

    public function deleteSelected(Request $request)
    {
        $this->authorize('isAdministrator');

        $user = User::whereIn('id',$request->user_ids)->delete();
        return response()->json([
            'status'=>200,
            'message'=>"Berhasil menghapus data user",
            'data'=>[]
        ]);
    }
}
