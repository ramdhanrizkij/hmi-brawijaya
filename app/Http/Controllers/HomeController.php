<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;
use App\Models\Berita;
use App\Models\Banner;

define('PUBLIKASI',1);

class HomeController extends Controller
{
    public function index()
    {
       
       
        $publikasi = Berita::where('kategori_id',PUBLIKASI)->orderBy('created_at','desc')
            ->with('tags','user')->where('status',2)->limit(3)->get();


        return view('welcome',compact('publikasi'));
    }

    public function kontak()
    {

        $banner = Slider::orderBy('created_at','desc')->get();
        $berita = Berita::where('kategori_id',BERITA_ID)->orderBy('created_at','desc')->where('status',2)->limit(3)->get();
        $liga = Berita::where('kategori_id',LIGA_KOMPETISI)->orderBy('created_at','desc')->where('status',2)->limit(5)->get();
        $populer = Berita::where('status',2)->orderBy('views_count','desc')->limit(8)->get();
        $video = Video::orderBy('created_at','desc')->get();
        $curahan = Berita::where('kategori_id',CURAHAN_HATI)->orderBy('created_at','desc')->where('status',2)->limit(5)->get();
        $tips = Berita::where('kategori_id',TIPS_RUGBY)->orderBy('created_at','desc')->where('status',2)->limit(5)->get();

        // Banner Iklan
        $homepage1 = Banner::where('kategori','homepage')->where('posisi',1)->first();
        $homepage2 = Banner::where('kategori','homepage')->where('posisi',2)->first();
        $homepage3 = Banner::where('kategori','homepage')->where('posisi',3)->first();
        $sidebar = Banner::where('kategori','sidebar')->where('posisi',1)->first();
        $bottom = Banner::where('kategori','bottom')->where('posisi',1)->first();

        return view('contact',compact('banner','berita','liga','populer','video','curahan','tips',
            'homepage1','homepage2','homepage3','sidebar','bottom'));
    }

    public function sitemap()
    {
        $kategoris = Kategori::all();
        $datas = [];
        foreach ($kategoris as $kat) { 
            $datas[$kat->nama_kategori] = Berita::where('kategori_id', '=', $kat->id)->get();
        }

        return view('sitemap', compact('datas'));
    }
}
