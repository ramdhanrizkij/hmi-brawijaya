<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BankData;
use App\Models\KategoriBankData;
use Illuminate\Support\Str;

class BankDataController extends Controller
{
    public function index(Request $request)
    {
        $datas = BankData::with('kategori')->orderBy('created_at')->get();
        return view('admin.bank_data.index',compact('datas'));
    }

    public function create(Request $request)
    {
        $kategori = KategoriBankData::orderBy('kategori','asc')->get();
        return view('admin.bank_data.create', compact('kategori'));
    }

    public function update(Request $request, $id)
    {
       $data = BankData::with('kategori')->findOrFail($id);
       $kategori = KategoriBankData::orderBy('kategori','asc')->get();
       return view('admin.bank_data.update', compact('data', 'kategori'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'kategori'=>'required',
            'file'=>'required',
            'judul_data'=>'required',
            'deskripsi'=>'required'
        ]);

        $models = new BankData;
        $models->kategori_id = $request->kategori;

        if($request->hasfile('file')){
            $featured = $request->file('file');
            $featuerd_new = time().$featured->getClientoriginalName();
            $featured->move('uploads/bank_data', $featuerd_new);
            $models->file = $featuerd_new;
        }

        $models->judul_data = $request->judul_data;
        $models->slug = Str::slug($request->slug);
        $models->deskripsi =$request->deskripsi;
        $models->tahun_terbit = $request->tahun_terbit;
        $models->penulis = $request->penulis;
        $models->penerbit = $request->penerbit;
        $models->download_count = 0;
        $models->save();
        return redirect('admin/bank-data')->with('success','Berhasil menambahkan data');
    }

    public function save(Request $request, $id)
    {
        $request->validate([
            'kategori'=>'required',
            'judul_data'=>'required',
            'deskripsi'=>'required'
        ]);

        $models = BankData::findOrFail($id);
        $models->kategori_id = $request->kategori;

        if($request->hasfile('file')){
            $featured = $request->file('file');
            $featuerd_new = time().$featured->getClientoriginalName();
            $featured->move('uploads/bank_data', $featuerd_new);
            $models->file = $featuerd_new;
        }

        $models->judul_data = $request->judul_data;
        $models->slug = Str::slug($request->slug);
        $models->deskripsi =$request->deskripsi;
        $models->tahun_terbit = $request->tahun_terbit;
        $models->penulis = $request->penulis;
        $models->penerbit = $request->penerbit;
        $models->save();
        return redirect('admin/bank-data')->with('success','Berhasil mengubah data');
    }

    public function delete($id)
    {
        $models = BankData::findOrFail($id);
        $models->delete();
        return redirect('admin/bank-data')->with('success','Berhasil menghapus data');
    }
}
