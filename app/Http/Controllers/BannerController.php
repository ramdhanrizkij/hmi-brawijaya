<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Banner;

class BannerController extends Controller
{
    public function index(Request $request)
    {
        $homepage1 = Banner::where('kategori','homepage')->where('posisi',1)->first();
        $homepage2 = Banner::where('kategori','homepage')->where('posisi',2)->first();
        $homepage3 = Banner::where('kategori','homepage')->where('posisi',3)->first();
        $sidebar = Banner::where('kategori','sidebar')->where('posisi',1)->first();
        $bottom = Banner::where('kategori','bottom')->where('posisi',1)->first();
        
        return view('admin.banner.index',compact('homepage1','homepage2','homepage3','sidebar','bottom'));
    }

    public function update(Request $request)
    {
        $posisi = $request->posisi;
        $kategori = $request->kategori;
        $models = Banner::where('posisi',$posisi)->where('kategori',$kategori)->first();
        if(!$models){
            $models = new Banner;
        }
        $models->kategori = $kategori;
        $models->posisi = $posisi;
        $models->link_url = $request->link;
        if($request->hasFile('banner'))
        {
            $image      = $request->file('banner');
            $fileName   = time().uniqid().".".$image->getClientOriginalExtension();
            $image->move('uploads/banner_iklan', $fileName);

            if(file_exists('uploads/banner_iklan/'.$models->banner_image) && $models->banner_image)
            {
                unlink('uploads/banner_iklan/'.$models->banner_image);
            }
    
            $models->banner_image = $fileName;
        }

       
        $models->save();
        return redirect()->back()->with('success','Berhasil memperbaharui banner');

    }
}
