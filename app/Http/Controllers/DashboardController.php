<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BankData;
use App\Models\Berita;
use App\Models\Gallery;
use App\Models\User;

class DashboardController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $bankData = BankData::count();
        $totalPost = Berita::count();
        $totalGallery = Gallery::count();
        $totalMember = User::count();
        $files = BankData::orderBy('created_at','desc')->take(5)->get();
        return view('admin.dashboard', compact('bankData', 'totalPost','totalGallery', 'totalMember','files'));
    }
}
