<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class ContactController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $contact = Contact::all();

        return view('admin.contact.index', compact('contact'));
    }

    public function save(Request $request)
    {
        $this->authorize('isAdministrator');

        $validate = $request->validate([
            'nama' => 'required',
            'email' => 'required|email',
            'pesan' => 'required',
        ]);

//        if(!$validate){
//            return redirect()->back()->withErrors($validate)->withInput();
//        }

        $id = $request->id ?? null;
        $contact = Contact::updateOrCreate(
            ['id' => $id],
            [
                'nama' => $request->nama,
                'email' => $request->email,
                'website' => $request->website ?? "#",
                'pesan' => $request->pesan,
            ]);

        return response()->json([
            'status' => 200,
            'message' => 'Pesan berhasil dikirim, terimakasih',
        ]);
    }


    public function tableAPI(Request $request)
    {
        $datas = Contact::latest()->get();
        return DataTables::of($datas)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                return '<div class="btn-group">
                              <button class="btn btn-sm btn-danger" data-id="'.$row['id'].'" id="deleteContact"><em class="icon ni ni-trash"></em> Delete</button>
                        </div>';
            })
            ->addColumn('checkbox', function($row){
                return '<input type="checkbox" name="contact_checkbox" data-id="'.$row['id'].'"><label></label>';
            })
            ->rawColumns(['action','checkbox'])
            ->make(true);
    }


    public function destroy(Request $request)
    {
        $this->authorize('isAdministrator');

        $id = $request->id;
        $contact = Contact::find($id);

        $contact->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Berhasil menghapus contact',
            'data' => []
        ]);
    }

    public function deleteSelected(Request $request)
    {
        $contact = Contact::whereIn('id', $request->contact_ids)->delete();
        return response()->json([
            'status' => 200,
            'message' => 'Berhasil menghapus data contact',
            'data' => []
        ]);
    }
}
