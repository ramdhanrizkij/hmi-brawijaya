<?php

namespace App\Http\Controllers;

use App\Models\SosialMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SosialMediaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->authorize('isAdministrator');

        $sosial = SosialMedia::all();
        return view('admin.sosial.index')->with('datas', $sosial);
    }

    public function save(Request $request)
    {
        $this->authorize('isAdministrator');

        $validate = $request->validate([
            'judul' => 'required',
            'image' => 'required_without:id',
        ]);

        if(!$validate){
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $id = $request->id ?? null;

        if($request->hasFile('image'))
        {
            $image      = $request->file('image');
            $fileName   = time().uniqid().".".$image->getClientOriginalExtension();
            $image->move('public/storage/sosial', $fileName);
        }

        if($id != null)
        {
            $sosial = SosialMedia::find($id);
            if($request->hasFile('image'))
            {
                if (file_exists(asset('public/storage/sosial/'.$sosial->image)) ){
                    unlink($sosial->image);
                }
                $sosial->image = $fileName;
            }
            $sosial->judul = $request->judul;
            $sosial->link = $request->link;

            $sosial->save();
        } else {
            $sosial = SosialMedia::create(
                [
                    'judul' => $request->judul,
                    'link' => $request->link ?? "#",
                    'image' => $fileName,
                ]
            );
        }


        Session::flash('success', "Berhasil menyimpan data");
        return redirect()->route('sosial.index');
    }

    public function destroy($id)
    {
        $this->authorize('isAdministrator');

        $sosial = SosialMedia::findOrFail($id);

        if(file_exists($sosial->image))
        {
            unlink($sosial->image);
        }
        $sosial->delete();
        Session::flash('success', "Berhasil menghapus data");
        return redirect()->route('sosial.index');
    }
}
