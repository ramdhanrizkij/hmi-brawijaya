<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->authorize('isAdministrator');

        $gallery = Gallery::all();
        return view('admin.gallery.index')->with('datas', $gallery);
    }

    public function save(Request $request)
    {
        $this->authorize('isAdministrator');

        $validate = $request->validate([
            'judul' => 'required',
            'image' => 'required_without:id',
        ]);

        if(!$validate){
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $id = $request->id ?? null;

        if($request->hasFile('image'))
        {
            $image      = $request->file('image');
            $fileName   = time().uniqid().".".$image->getClientOriginalExtension();
            $image->move('uploads/gallery', $fileName);
        }

        if($id != null)
        {
            $gallery = Gallery::find($id);
            if($request->hasFile('image'))
            {
                if (file_exists(asset('public/storage/gallery/'.$gallery->image)) ){
                    unlink($gallery->image);
                }
                $gallery->image = $fileName;
            }
            $gallery->judul = $request->judul;
            $gallery->link = $request->link;

            $gallery->save();
        } else {
            $gallery = Gallery::create(
                [
                    'judul' => $request->judul,
                    'link' => $request->link ?? "#",
                    'image' => $fileName,
                ]
            );
        }


        Session::flash('success', "Berhasil menyimpan data");
        return redirect()->route('gallery.index');
    }

    public function destroy($id)
    {
        $this->authorize('isAdministrator');

        $gallery = Gallery::findOrFail($id);

        if(file_exists($gallery->image))
        {
            unlink($gallery->image);
        }
        $gallery->delete();
        Session::flash('success', "Berhasil menghapus data");
        return redirect()->route('gallery.index');
    }
}
