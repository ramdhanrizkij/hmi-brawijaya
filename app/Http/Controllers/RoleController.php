<?php

namespace App\Http\Controllers;

//use App\DataTables\RoleDataTable;
use App\Models\Role;
use App\Models\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;

class RoleController extends Controller
{
//    public function index(RoleDataTable $dataTable)
    public function index()
    {
        $this->authorize('isAdministrator');

        return view('admin.role.index');
    }

    public function findById($id)
    {
        $this->authorize('isAdministrator');

        $role = Role::findOrFail($id);

        return response()->json($role);
    }

    public function tableAPI(Request $request)
    {
        $this->authorize('isAdministrator');

        $datas = Role::latest()->get();
        return DataTables::of($datas)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                return '<div class="btn-group">
                              <button class="btn btn-sm btn-success" data-id="'.$row['id'].'" id="updateRole"><em class="icon ni ni-edit"></em></button>
                              <button class="btn btn-sm btn-danger" data-id="'.$row['id'].'" id="deleteRole"><em class="icon ni ni-trash"></em></button>
                        </div>';
            })
            ->addColumn('checkbox', function($row){
                return '<input type="checkbox" name="role_checkbox" data-id="'.$row['id'].'"><label></label>';
            })
            ->rawColumns(['action','checkbox'])
            ->make(true);
    }

    public function save(Request $request)
    {
        $this->authorize('isAdministrator');
        $validate = $request->validate([
            'role_name' => 'required',
        ]);

        if(!$validate){
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $id = $request->id ?? null;
        $role = Role::updateOrCreate(
            ['id' => $id],
            [
                'role_name' => $request->role_name,
            ]
        );

        return response()->json([
            'status' => 200,
            'message' => 'Data Role berhasil disimpan',
            'data' => $role,
        ]);
    }

    public function destroy(Request $request)
    {
        $this->authorize('isAdministrator');

        $id = $request->id;
        $this->authorize('isAdministrator');
        $userRole = User::where('role_id', $id)->count();
        if($userRole == 0){
            $role = Role::find($id);
            $role->delete();

            return response()->json([
                'status' => 200,
                'message' => 'Berhasil menghapus role',
                'data' => []
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'role masih digunakan oleh user',
                'data' => []
            ]);
        }
    }

    public function deleteSelected(Request $request)
    {
        $this->authorize('isAdministrator');

        $role = Role::whereIn('id', $request->role_ids)->delete();
        return response()->json([
            'status' => 200,
            'message' => 'Berhasil menghapus data role',
            'data' => []
        ]);
    }
}
