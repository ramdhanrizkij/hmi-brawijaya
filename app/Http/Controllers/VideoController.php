<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class VideoController extends Controller
{

    public function index()
    {
        if(! \Gate::any(['isAdministrator', 'isEditor'])){
            abort('403');
        }

        return view('admin.video.index');
    }

    public function tableAPI(Request $request)
    {
        $datas = Video::latest()->get();
        return DataTables::of($datas)
            ->addIndexColumn()
            ->addColumn('video_url', function ($row){
                return '<a href="'.$row['video_url'].'" target="_blank">'.$row['video_url'].'</a>';
            })
            ->addColumn('action', function($row){
                return '<div class="btn-group">
                              <button class="btn btn-sm btn-success" data-id="'.$row['id'].'" id="updateVideo"><em class="icon ni ni-edit"></em> Update</button>
                              <button class="btn btn-sm btn-danger" data-id="'.$row['id'].'" id="deleteVideo"><em class="icon ni ni-trash"></em> Delete</button>
                        </div>';
            })
            ->addColumn('checkbox', function($row){
                return '<input type="checkbox" name="video_checkbox" data-id="'.$row['id'].'"><label></label>';
            })
            ->rawColumns(['video_url','action','checkbox'])
            ->make(true);
    }

    public function save(Request $request)
    {
        if(! \Gate::any(['isAdministrator', 'isEditor'])){
            abort('403');
        }

        $validate = $request->validate([
            'judul' => 'required',
            'video_url' => 'required|url',
        ]);

        if(!$validate){
            return redirect()->back()->withErrors($validate)->withInput();
        }

        $id = $request->id ?? null;
        $viewCount = $request->view_count ?? 0;
        $status = $request->status ?? 'UNACTIVE';
        $video = Video::updateOrCreate(
            ['id' => $id],
            [
            'judul' => $request->judul,
            'video_url' => $request->video_url,
            'description' => $request->description,
            'user_id' => \Auth::user()->id,
            'view_count' => $viewCount,
            'status' => $status,
        ]);

        Session::flash('success', "Berhasil menyimpan data Video");
        return redirect()->route('video.index');
    }

    public function destroy(Request $request)
    {
        if(! \Gate::any(['isAdministrator', 'isEditor'])){
            abort('403');
        }

        $id = $request->id;
        $video = Video::findOrFail($id);

        $video->delete();
        Session::flash('success', "Berhasil menghapus data video");
        return redirect()->route('video.index');
    }

    public function deleteSelected(Request $request)
    {
        $video = Video::whereIn('id', $request->video_ids)->delete();
        return response()->json([
            'status' => 200,
            'message' => 'Berhasil menghapus data video',
            'data' => []
        ]);
    }
}
