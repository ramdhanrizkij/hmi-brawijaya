<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Str;

class KategoriController extends Controller
{
    public function index(Request $request)
    {
        $kategori = Kategori::whereNull('parent_id')->orderBy('nama_kategori','asc')->get();
        return view('admin.kategori.index',compact('kategori'));
    }

    public function tableAPI(Request $request)
    {
        $data = Kategori::whereNull('parent_id')->get();
        $data = $data->toArray();
        $added = 0;
        foreach($data as $key=>$item) {
            $child = Kategori::where('parent_id',$item['id'])->get();
            foreach($child as $key2=>$val) {
                $val['nama_kategori'] = $val['nama_kategori'];
                $added +=1;
                $prev = array_slice($data, 0, $added+$key);
                $rest = array_slice($data, $added+$key);
                $data = array_merge($prev,array($val),$rest);
            }
        }

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('nama_kategori', function($row){
                if($row['parent_id']) {
                    return "<span class='ml-3'>".$row['nama_kategori']."</span>";
                }else {
                    return "<b>".$row['nama_kategori']."</b>";
                }
            })
            ->addColumn('action', function($row){
                return '<div class="btn-group">
                              <button class="btn btn-sm btn-success" data-id="'.$row['id'].'" id="updateKategori">Update</button>
                              <button class="btn btn-sm btn-danger" data-id="'.$row['id'].'" id="deleteKategori">Delete</button>
                        </div>';
            })
            ->addColumn('checkbox', function($row){
                return '<input type="checkbox" name="kategori_checkbox" data-id="'.$row['id'].'"><label></label>';
            })
            ->rawColumns(['action','checkbox','nama_kategori'])
            ->make(true);
    }

    public function create(Request $request)
    {
        $request->validate([
            'kategori' => 'required|max:255',
        ]);
        $kategori = new Kategori;
        $kategori->nama_kategori = $request->kategori;
        $kategori->slug = Str::slug($request->kategori, '-');
        $kategori->parent_id = $request->get('parent_id',null);
        $kategori->save();
        return response()->json([
            'status'=>200,
            'message'=>'Data kategori berhasil ditambahkan',
            'data'=>[]
        ]);
    }

    public function delete(Request $request)
    {
        $kategori = Kategori::findOrFail($request->get('id'));
        $kategori->delete();
        return response()->json([
            'status'=>200,
            'message'=>'Berhasil menghapus data kategori',
            'result'=>[]
        ]);
    }

    public function getById(Request $request,$id)
    {
        $kategori= Kategori::findOrFail($id);
        return response()->json($kategori);
    }

    public function update(Request $request)
    {
        $kategori = Kategori::findOrFail($request->get('id'));
        $kategori->nama_kategori = $request->kategori;
        $kategori->slug = Str::slug($request->kategori, '-');
        $kategori->save();
        return response()->json([
            'status'=>200,
            'message'=>'Data kategori berhasil diubah',
            'data'=>[]
        ]);
    }

    public function deleteSelected(Request $request)
    {
        $kategori = Kategori::whereIn('id',$request->kategori_ids)->delete();
        return response()->json([
            'status'=>200,
            'message'=>"Berhasil menghapus data kategori",
            'data'=>[]
        ]);
    }
}
