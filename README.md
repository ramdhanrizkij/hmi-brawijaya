## About Project

Project ini merupakan project Company Profile dan Media Berita dari situs mainrugby.com

-   Laravel 8.0
-   Boostrap
-   Ajax

## Features

Fitur yang terdapat pada aplikasi ini adalah :

-   Managemen USER
-   Multi User Authentication
-   Kontributor dapat membuat berita baru
-   Editor dapat mereview dan melakukan publish berita
-   Managemen Halaman
-   Pengaturan Menu
-   Halaman utama berita

## License

This Project Lisensed Under the [Commercial software]
