@extends('layouts.app')
@section('content')
<div class="container space-20 space-padding-tb-20">
    <ul class="breadcrumb">
        <li><a href="{{url('/')}}">Home</a></li>
        <li class="active">{{isset($kategori)?$kategori->nama_kategori:'Berita'}}</li>
    </ul>
</div>
<!-- End container -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="single-post">
                <div class="blog-post-item cat-1 box">
                    <div class="title-v1 box">
                        <h3>{{isset($kategori)?$kategori->nama_kategori:'Berita'}}</h3>
                    </div>
                    <!-- End title -->
                    <div class="row">
                        @foreach($berita as $key=>$item)
                        <div class="col-md-4 col-sm-6">
                            <div class="post-item ver3 overlay space-30">
                                <div class="wrap-images">
                                    <a class="images" href="{{url('berita/'.$item->slug)}}" title="images"><img class='img-responsive'
                                            src="{{asset('uploads/berita/'.$item->thumbnail)}}" alt="images"></a>
                                </div>
                                <div class="text">
                                    <h2><a href="" title="title">{{$item->judul}}</a></h2>
                                    <div class="tag">
                                        <p class="date"><i class="fa fa-clock-o"></i>{{$item->created_at->format('M d, Y')}}</p>
                                    </div>
                                    <p>{{substr($item->meta_description,0,120)}}...</p>
                                    <a class="read-more" href="{{url('berita/'.$item->slug)}}" title="read more">read more</a>
                                </div>
                            </div>
                            <!-- End item -->
                        </div>
                        @endforeach
                        </div>
                        <!-- End col-md-4 -->
                    </div>
                    <!-- End row -->
                </div>
            </div>
            <!-- End signle-post -->
        </div>
    </div>
</div>
<div id="back-to-top">
    <i class="fa fa-long-arrow-up"></i>
</div>
<div class="box center float-left space-60">
    {{$berita->links('vendor.pagination.custom')}}
    
    <!-- End pagination -->
</div>
@endsection
