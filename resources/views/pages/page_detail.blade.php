@extends('layouts.app')
@section('content')
<div class="head-single">
    <div class="blog-post-images">
        @if($page->thumbnail=='noimage.png')
        <a href="#" title="Post"><img class="img-responsive" src="{{asset('assets/images/single-post-stan-v2.jpg')}}"
                alt="banner"></a>
        @else
        <a href="#" title="Post"><img class="img-responsive" src="{{asset('uploads/page/'.$page->thumbnail)}}"
            style="width:100%;max-height: 487px;object-fit: cover;"
                alt="banner"></a>
        @endif
    </div>
</div>
<div class="container space-20 space-padding-tb-20">
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li class="active">{{$page->judul}}</li>
    </ul>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="single-post">
                <div class="blog-post-item cat-1 box">
                    <div class="content">
                        <h3>{{$page->judul}}</h3>
                        <div class="tag">
                        </div>
                        {!! $page->content !!}
                    </div>
                    <!-- End slider -->
                    <!-- End title -->
                    <!-- End title -->
                    <!-- End box -->
                </div>
            </div>
            <!-- End signle-post -->
        </div>
    </div>
</div>

@endsection
