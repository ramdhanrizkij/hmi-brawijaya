@extends('layouts.app')
@section('content')
<div class="container space-20 space-padding-tb-20">
    <ul class="breadcrumb">
        <li><a href="{{url('/')}}">Home</a></li>
        <li><a href="{{url('/kategori/berita')}}">Berita</a></li>
        <li class="active">{{$berita->judul}}</li>
    </ul>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="single-post">
                <div class="blog-post-item cat-1 box nav-white">
                    <div class="blog-post-images">
                        <img src="{{asset('uploads/berita/'.$berita->thumbnail)}}" style="max-width: 770px;max-height: 480px;"/>
                        <div class="content">
                            <h3>{{$berita->judul}}</h3>
                            <div class="tag">
                                <p class="date"><i class="fa fa-clock-o"></i>{{$berita->created_at->format('M d,Y')}}
                                </p>
                            </div>
                            <div class="content">
                                {!! $berita->content !!}
                            </div>
                        </div>
                        <p class="tag-cat"><span>tag:</span>
                            @foreach($berita->tags as $item)
                            <a href="{{url('tags/'.$item->slug)}}" title="{{$item->tag}}">{{$item->tag}}</a>,
                            @endforeach
                        </p>
                        <div class="box-user space-padding-tb-40">
                            <div class="box">
                                <div class="avatar">
                                    <img class="img-responsive" src="{{asset('frontend/images/about-single.jpg')}}" alt="avatar">
                                </div>
                                <div class="text">
                                    <h3>Zaskia Sungkar <span class="user-level">Contributor Jawa Barat</span></h3>
                                    <p>Anim tincidunt odio massa esse per. Nisl neque iaculis ad urna non. Metus
                                        vestibulum
                                        tortor occaecat dolor. Hendrerit euismod quam excepteur ut. Mollis ante nulla
                                        nibh
                                        ex...</p>
                                </div>
                            </div>
                        </div>
                        <!-- End box-user -->
                        <div class="pagination">
                            @if($prev)
                            <div class="prev">
                                <a href="{{url('berita/'.$prev->slug)}}" title="prev">
                                    <div class="icon-box">
                                        <i class="icon"></i>
                                    </div>
                                    <div class="text">
                                        <p class="control">PREVIOUS POST</p>
                                        <p class="title">{{$prev->judul}}</p>
                                    </div>
                                </a>
                            </div>
                            @endif
                            @if($next)
                            <div class="next">
                                <a href="{{url('berita/'.$next->slug)}}" title="next">
                                    <div class="text">
                                        <p class="control">NEXT POST</p>
                                        <p class="title">{{$next->judul}}
                                        </p>
                                    </div>
                                    <div class="icon-box">
                                        <i class="icon"></i>
                                    </div>
                                </a>
                            </div>
                            @endif
                        </div>
                        <!-- End pagination -->
                        <div class="title-v1 box">
                            <h3>YOU MAY ALSO LIKE</h3>
                        </div>
                        <!-- End title -->
                        <div class="slider-two-item box float-left space-40 nav-ver2 nav-white">
                            @foreach($related as $key=>$item)
                            <div class="post-item ver3 overlay">
                                <div class="wrap-images">
                                    <a class="images" href="{{url('berita/'.$item->slug)}}" title="images"><img class='img-responsive'
                                            src="{{asset('uploads/berita/'.$item->thumbnail)}}" alt="images"></a>
                                </div>
                                <div class="text">
                                    <h2><a href="{{url('berita/'.$item->slug)}}" title="title">{{$item->judul}}</a></h2>
                                    <div class="tag">
                                        <p class="date"><i class="fa fa-clock-o"></i>{{$item->created_at->format('M d,Y')}}</p>
                                    </div>
                                    <p>{{substr($item->meta_description,0,110)}}...</p>
                                    <a class="read-more" href="{{url('berita/'.$item->slug)}}" title="read more">read more</a>
                                </div>
                            </div>
                            @endforeach
                            <!-- End item -->
                        </div>
                        <!-- End slider -->
                    </div>
                </div>
            </div>
        </div>
                <!-- End signle-post -->

        <div class="col-md-4">
            <aside class="widget popular">
                <h3 class="widget-title">Terpopuler</h3>
                <div class="content">
                    @foreach($populer as $key=>$item)
                    @if($key==0)
                    <div class="post-item ver1 overlay">
                        <a class="images" href="{{url('berita/'.$item->slug)}}" title="images"><img class='img-responsive'
                                src="{{asset('uploads/berita/'.$item->thumbnail)}}" alt="images"></a>
                        <div class="text">
                            <h2><a href="{{url('berita/'.$item->slug)}}" title="title">{{$item->judul}}</a></h2>
                            <div class="tag">
                                <p class="date"><i class="fa fa-clock-o"></i>{{$item->created_at->format('M d,Y')}}</p>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="post-item ver2">
                        <a class="images" href="{{url('berita/'.$item->slug)}}" title="images"><img class='img-responsive'
                                src="{{asset('uploads/berita/'.$item->thumbnail)}}" alt="images"></a>
                        <div class="text">
                            <h2><a href="{{url('berita/'.$item->slug)}}" title="title">{{$item->judul}}</a>
                            </h2>
                            <div class="tag">
                                <p class="date"><i class="fa fa-clock-o"></i>{{$item->created_at->format('M d,Y')}}</p>
                            </div>
                        </div>
                    </div>
                    <!-- End item -->
                    @endif
                    <!-- End item -->
                    @endforeach
                </div>
            </aside>
            <aside class="widget">
                <div class="banner">
                    <img class="img-responsive" src="{{asset('frontend/images/widget-banner.jpg')}}" alt="banner">
                </div>
            </aside>
        </div>
           
    </diV>
</div>

    @endsection
