@extends('layouts.app')
@section('content')
@include('layouts.partials.home.hero')
 <!--course section start-->
 <section class="blog section-padding">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-heading center-heading">
                    <h3>Potret Agenda</h3>
                </div>
            </div>
        </div>

       
        <div class="row">               
            @foreach($publikasi as $key=>$item)
            <div class="col-lg-4 col-md-6">
                <div class="blog-item">
                    <img src="{{asset('uploads/berita/'.$item->thumbnail)}}" alt="" class="img-fluid" style="height:190px; width:100%; object-fit:cover">
                    <div class="blog-content"  style="min-height:328px;">
                        <div class="entry-meta">
                            <span><i class="fa fa-calendar-alt"></i>{{$item->created_at->format('d M Y')}}</span>
                            <span><i class="fa fa-eye"></i>{{$item->views_count}}</span>
                        </div>
    
                        <h2><a href="#">{{$item->judul}}</a></h2>
                        <p>{{substr($item->meta_description,0,100)}}..</p>
                        <a href="#" class="btn btn-main btn-small"><i class="fa fa-plus-circle mr-2"></i>Read More</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>



<section class="section-padding popular-course bg-grey">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="section-heading">
                    <span class="subheading">Publikasi</span>
                    <h3>Daftar Publikasi Terbaru</h3>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="course-btn text-lg-right"><a href="#" class="btn btn-main">Lainnya</a></div>
            </div>
        </div>

        <div class="row">
        @foreach($publikasi as $key=>$item)
            <div class="col-lg-4 col-md-6">
                <div class="course-block">
                    <div class="course-img">
                        <img src="{{asset('uploads/berita/'.$item->thumbnail)}}" alt="" class="img-fluid" style="width: 100%;object-fit: cover;height: 260px;">
                        <span class="course-label">{{$item->tags?$item->tags[0]->tag:''}}</span>
                    </div>
                    
                    <div class="course-content"> 
                        
                        <h4><a href="#">{{$item->judul_berita}}</a></h4>    
                        <p>Oleh : {{$item->user->name}}</p>

                        <div class="course-footer d-lg-flex align-items-center justify-content-between">
                            <div class="course-meta" style="display:flex;justify-content:between">
                                <span class="course-student"><i class="fa fa-eye"></i>{{$item->views_count}}</span>
                                <span class="course-duration"><i class="fa fa-calendar"></i>{{$item->created_at->format('d M Y')}}</span>
                            </div> 
                            <div class="buy-btn"><a href="#" class="btn btn-main btn-small">Detail</a></div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<section class="section-padding category-section">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-lg-6">
                <div class="section-heading center-heading">
                    <span class="subheading"></span>
                    <h3>Gallery Kegiatan</h3>
                </div>
            </div>
        </div>

        <div class="row no-gutters">
            <div class="col-lg-3 col-md-6">
                <div class="course-category style-1">
                   <div class="category-icon">
                        <i class="bi bi-laptop"></i>
                   </div>
                    <h4><a href="#">Web Development</a></h4>
                    <p>4 Courses</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="course-category style-2">
                    <div class="category-icon">
                        <i class="bi bi-layer"></i>
                    </div>
                    <h4><a href="#">Design</a></h4>
                    <p>12 Courses</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="course-category style-3">
                    <div class="category-icon">
                        <i class="bi bi-target-arrow"></i>
                    </div>
                    <h4><a href="#">Marketing</a></h4>
                    <p>6 Courses</p>
                </div>
            </div>

             <div class="col-lg-3 col-md-6">
                <div class="course-category style-4">
                    <div class="category-icon">
                        <i class="bi bi-rocket2"></i>
                    </div>
                    <h4><a href="#">Art & Design</a></h4>
                    <p>6 Courses</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="course-category style-2">
                    <div class="category-icon">
                        <i class="bi bi-shield"></i>
                    </div>
                    <h4><a href="#">Design</a></h4>
                    <p>12 Courses</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="course-category style-1">
                   <div class="category-icon">
                        <i class="bi bi-slider-range"></i>
                   </div>
                    <h4><a href="#">Web Development</a></h4>
                    <p>4 Courses</p>
                </div>
            </div>
           
            <div class="col-lg-3 col-md-6">
                <div class="course-category style-4">
                    <div class="category-icon">
                        <i class="bi bi-bulb"></i>
                    </div>
                    <h4><a href="#">Art & Design</a></h4>
                    <p>6 Courses</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="course-category style-3">
                    <div class="category-icon">
                        <i class="bi bi-android"></i>
                    </div>
                    <h4><a href="#">Marketing</a></h4>
                    <p>6 Courses</p>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="text-center mt-5">
                    <div class="course-btn mt-4"><a href="#" class="btn btn-main"><i class="fa fa-grip-horizontal mr-2"></i>Selengkapnya</a></div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection