@extends('layouts.app')
@section('content')
<?php
        $dataLength = count($datas);
        $half = floor($dataLength / 2);
    ?>
<div class="container space-20 space-padding-tb-20">
    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="active">Mencari</li>
    </ul>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="single-post">
                <div class="blog-post-item cat-1 box">
                    <div class="box-user space-30">
                    </div>
                    <div class="title-v1 box">
                        <h3>Hasil Pencarian : <b>"{{ Request::get('search') }}"</b></h3>
                    </div>
                    <!-- End title -->
                    <div class="technnology box space-30">
                        @foreach($datas as $key => $row)
                        <div class="post-item cat-1 ver2">
                            <a class="images" href="{{ url('/berita')."/".$row->slug }}" title="images"><img
                                    class='img-responsive' src="{{ asset('uploads/berita/'.$row->thumbnail) }}"
                                    alt="images"></a>
                            <div class="text">
                                <h2><a href="" title="title">{{ $row->judul }}</a></h2>
                                <div class="tag">
                                    @foreach($row->tags as $tag)
                                    <span class="lable">{{ $tag->tag }}</span>
                                    @endforeach
                                    <p class="date"><i
                                            class="fa fa-clock-o"></i>{{ date_format($row->created_at, "M d, Y") }}</p>
                                </div>
                                <p class="description">{!! substr($row->content, 0, 400)."... " !!}</p>
                                <a class="read-more" href="#" title="readMore">Read More</a>
                            </div>
                        </div>
                        <!-- End item -->
                        @if(sizeof($datas) > 3 && $half == $key)
                    </div>
                    <div class="banner box float-left space-padding-tb-30 space-30">
                        <div class="col-md-6 col-sm-6 align-right">
                            <img class="img-responsive" src="{{ asset("frontend/images/category-v1-banner1.jpg")}}"
                                alt="images">
                        </div>
                        <div class="col-md-6 col-sm-6 align-left">
                            <img class="img-responsive" src="{{ asset("frontend/images/category-v1-banner1.jpg")}}"
                                alt="images">
                        </div>
                    </div>
                    <!-- End banner -->
                    <div class="technnology box space-30">
                        @endif
                        @endforeach
                    </div>

                </div>
            </div>
            <!-- End signle-post -->
            <div class="box center float-left space-30">
                <nav class="pagination">
                    {{ $datas->links('components.pagination') }}
                </nav>
                <!-- End pagination -->
            </div>
            <!-- End float-left -->
        </div>

        @if(sizeof($populer)>0)
        <div class="col-md-4">
            <aside class="widget popular">
                <h3 class="widget-title">Terpopuler</h3>
                <?php $item = $populer[0];?>
                <div class="content">
                    <div class="post-item ver1 overlay">
                        <a class="images" href="{{url('/berita/'.$item->slug)}}" title="images"><img
                                class='img-responsive' src="{{asset('uploads/berita/'.$item->thumbnail)}}"
                                alt="images"></a>
                        <div class="text">
                            <h2><a href="{{url('berita/'.$item->slug)}}" title="title">{{$item->judul}}</a></h2>
                            <div class="tag">
                                <p class="date"><i class="fa fa-clock-o"></i>{{$item->created_at->format('M d,Y')}}</p>
                            </div>
                        </div>
                    </div>
                    <!-- End item -->
                    @foreach($populer as $key=>$item)
                    @if($key>0)
                    <div class="post-item ver2">
                        <a class="images" href="{{url('/berita/'.$item->slug)}}" title="images"><img
                                class='img-responsive' src="{{asset('uploads/berita/'.$item->thumbnail)}}"
                                alt="images"></a>
                        <div class="text">
                            <h2><a href="{{url('berita/'.$item->slug)}}" title="title">{{$item->judul}}</a></h2>
                            <div class="tag">
                                <p class="date"><i class="fa fa-clock-o"></i>{{$item->created_at->format('M d, Y')}}</p>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </aside>
            <aside class="widget">
                <div class="banner">
                    @if($sidebar && $sidebar->banner_image)
                    <img class="img-responsive" src="{{asset('uploads/banner_iklan/'.$sidebar->banner_image)}}"
                        alt="banner">
                    @else
                    <img class="img-responsive" src="{{asset('frontend/images/widget-banner.jpg')}}" alt="banner">
                    @endif

                </div>
            </aside>
        </div>
        @endif
    </div>
</div>
<!-- End container -->

<!-- End box-bottom -->
<div id="back-to-top">
    <i class="fa fa-long-arrow-up"></i>
</div>
@endsection
