<!DOCTYPE html>
<html lang="zxx">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="description" content="edutim,coaching, distant learning, education html, health coaching, kids education, language school, learning online html, live training, online courses, online training, remote training, school html theme, training, university html, virtual training  ">
  
  <meta name="author" content="themeturn.com">

  <title>HMI Hukum Brawijaya</title>

  <!-- Mobile Specific Meta-->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- bootstrap.min css -->
  <link rel="stylesheet" href="{{ asset('frontend/vendors/bootstrap/bootstrap.css')}}">
  <!-- Iconfont Css -->
  <link rel="stylesheet" href="{{ asset('frontend/vendors/fontawesome/css/all.css')}}">
  <link rel="stylesheet" href="{{ asset('frontend/vendors/bicon/css/bicon.min.css')}}">
  <link rel="stylesheet" href="{{ asset('frontend/vendors/themify/themify-icons.css')}}">
  <!-- animate.css -->
  <link rel="stylesheet" href="{{ asset('frontend/vendors/animate-css/animate.css')}}">
  <!-- WooCOmmerce CSS -->
  <link rel="stylesheet" href="{{ asset('frontend/vendors/woocommerce/woocommerce-layouts.css')}}">
  <link rel="stylesheet" href="{{ asset('frontend/vendors/woocommerce/woocommerce-small-screen.css')}}">
  <link rel="stylesheet" href="{{ asset('frontend/vendors/woocommerce/woocommerce.css')}}">
   <!-- Owl Carousel  CSS -->
  <link rel="stylesheet" href="{{ asset('frontend/vendors/owl/assets/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{ asset('frontend/vendors/owl/assets/owl.theme.default.min.css')}}">

  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="{{ asset('frontend/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('frontend/css/responsive.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>

<body id="top-header">

  

    
<header>
    <div class="header-top">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6">
                    <ul class="header-contact">
                        <li>
                            <span>Call :</span>
                           +23 45 5467
                        </li>
                        <li>
                            <span>Email :</span>
                            contact@hmibrawijaya.com
                        </li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="header-right float-right">
                        <div class="header-socials">
                            <ul>
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                            </ul>
                        </div>
                        <div class="header-btn">
                            <a href="#" class="btn btn-main btn-small"><i class="fa fa-user mr-2"></i>Login Anggota</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>

    <!-- Main Menu Start -->
   
    <div class="site-navigation main_menu " id="mainmenu-area">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <a class="navbar-brand" href="index.html">
                    <img src="{{ asset('frontend/images/logo-dark.png')}}" alt="HMI Hukum Brawijaya" class="img-fluid" width="80%">
                </a>

                <!-- Toggler -->

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>

                <!-- Collapse -->
                <div class="collapse navbar-collapse" id="navbarMenu">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item ">
                            <a href="{{url('/')}}" class="nav-link js-scroll-trigger">
                                Home
                            </a>
                        </li>

                        <li class="nav-item ">
                            <a href="{{url('/kategori/publikasi')}}" class="nav-link js-scroll-trigger">
                                Publikasi
                            </a>
                        </li>

                        <li class="nav-item ">
                            <a href="{{url('/kategori/info-kegiatan')}}" class="nav-link js-scroll-trigger">
                                Info Kegiatan
                            </a>
                        </li>

                        <li class="nav-item ">
                            <a href="{{url('/gallery')}}" class="nav-link js-scroll-trigger">
                                Gallery
                            </a>
                        </li>

                        <li class="nav-item ">
                            <a href="{{url('/gallery')}}" class="nav-link js-scroll-trigger">
                                Produk Kami
                            </a>
                        </li>
                        
                        <li class="nav-item ">
                            <a href="contact.html" class="nav-link">
                                Contact
                            </a>
                        </li>
                    </ul>

                    <ul class="header-contact-right d-none d-lg-block">
                        <li><a href="#" class="header-search search_toggle"> <i class="fa fa fa-search"></i></a></li>
                    </ul>
                   
                </div> <!-- / .navbar-collapse -->
            </div> <!-- / .container -->
        </nav>
    </div>
</header>
