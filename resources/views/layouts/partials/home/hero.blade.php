<section class="banner">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-8">
                <div class="banner-content center-heading">

                    <h1>Selamat datang di <font color="#009787">SIT</font> - HMI</h1>
                    <h5 style="font-weight:bold;color:white;margin-top:-35px;"><font color="#009787">SIT</font> - HMI merupakan pelayanan khusus untuk memfasilitasi kader dalam mengakses layanan yang disediakan oleh HMI Hukum Brawijaya</h5>
                   
                </div>
            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>


<section class="feature">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-4 col-md-6">
                <div class="feature-item">
                    <div class="feature-icon banner-icon">
                        01
                    </div>
                    <div class="feature-text">
                        <h4>Beranda Kader</h4>
                        <p>Layanan HMI Hukum Brawijaya dalam memberi akses khusus member</p>
                    </div>
                </div>
            </div>
             <div class="col-lg-4 col-md-6">
                <div class="feature-item">
                    <div class="feature-icon banner-icon">
                        02
                    </div>
                    <div class="feature-text">
                        <h4>Produk HMI</h4>
                        <p>Produk-produk merchandise yang disediakan HMI hukum Brawijaya </p>
                    </div>
                </div>
            </div>
             <div class="col-lg-4 col-md-6">
                <div class="feature-item">
                    <div class="feature-icon banner-icon">
                       03
                    </div>
                    <div class="feature-text">
                        <h4>Dasi HMI</h4>
                        <p>Layanan dapud untuk kader dalam menyediakan akses data dan publikasi</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
