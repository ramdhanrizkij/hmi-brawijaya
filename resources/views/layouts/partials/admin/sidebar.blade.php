 <!-- sidebar @s -->
 <div class="nk-sidebar nk-sidebar-fixed is-dark " data-content="sidebarMenu">
     <div class="nk-sidebar-element nk-sidebar-head">
         <div class="nk-menu-trigger">
             <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em
                     class="icon ni ni-arrow-left"></em></a>
             <a href="#" class="nk-nav-compact nk-quick-nav-icon d-none d-xl-inline-flex" data-target="sidebarMenu"><em
                     class="icon ni ni-menu"></em></a>
         </div>
         <div class="nk-sidebar-brand">
             <a href="{{url('/admin')}}" class="logo-link nk-sidebar-logo">
                <img src="{{asset('frontend/images/logo-white.png')}}" width="60%">
             </a>
         </div>
     </div><!-- .nk-sidebar-element -->
     <div class="nk-sidebar-element nk-sidebar-body">
         <div class="nk-sidebar-content">
             <div class="nk-sidebar-menu" data-simplebar>
                 <ul class="nk-menu">
                     <li class="nk-menu-item">
                         <a href="{{url('/admin')}}" class="nk-menu-link">
                             <span class="nk-menu-icon"><em class="icon ni ni-menu-squared"></em></span>
                             <span class="nk-menu-text">Dashboard</span>
                         </a>
                     </li><!-- .nk-menu-item -->
                     <li class="nk-menu-heading">
                         <h6 class="overline-title text-primary-alt">Content Management</h6>
                     </li><!-- .nk-menu-heading -->
                     <li class="nk-menu-item has-sub">
                         <a href="#" class="nk-menu-link nk-menu-toggle">
                             <span class="nk-menu-icon"><em class="icon ni ni-card-view"></em></span>
                             <span class="nk-menu-text">Postingan</span>
                         </a>
                         <ul class="nk-menu-sub">
                             <li class="nk-menu-item">
                                 <a href="{{url('admin/berita')}}" class="nk-menu-link"><span
                                         class="nk-menu-text">Daftar Post</span></a>
                             </li>
                             @if(Auth::user()->role_id==1)
                             <li class="nk-menu-item">
                                 <a href="{{url('admin/categories')}}" class="nk-menu-link"><span
                                         class="nk-menu-text">Kategori</span></a>
                             </li>
                             @endif
                             <li class="nk-menu-item">
                                 <a href="{{url('admin/berita/create')}}" class="nk-menu-link"><span
                                         class="nk-menu-text">Buat Post</span></a>
                             </li>
                         </ul><!-- .nk-menu-sub -->
                     </li><!-- .nk-menu-item -->
                     @if(Auth::user()->role_id==1)
                     <li class="nk-menu-item has-sub">
                         <a href="#" class="nk-menu-link nk-menu-toggle">
                             <span class="nk-menu-icon"><em class="icon ni ni-card-view"></em></span>
                             <span class="nk-menu-text">Product</span>
                         </a>
                         <ul class="nk-menu-sub">
                             <li class="nk-menu-item">
                                 <a href="{{url('admin/product')}}" class="nk-menu-link"><span
                                         class="nk-menu-text">Daftar Product</span></a>
                                </li>
                                
                                <li class="nk-menu-item">
                                    <a href="{{url('admin/product/kategori')}}" class="nk-menu-link"><span
                                            class="nk-menu-text">Kategori Product</span></a>
                                </li>
                         </ul><!-- .nk-menu-sub -->
                         
                     </li><!-- .nk-menu-item -->
                     @endif 
                     @if(Auth::user()->role_id==1)
                     <li class="nk-menu-item">
                        <a href="{{ route('gallery.index') }}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-instagram"></em></span>
                            <span class="nk-menu-text">Gallery</span>
                        </a>
                    </li>
                    <li class="nk-menu-item">
                        <a href="{{ route('bank_data.index') }}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-file-doc"></em></span>
                            <span class="nk-menu-text">Bank Data</span>
                        </a>
                    </li>
                     <li class="nk-menu-heading">
                         <h6 class="overline-title text-primary-alt">Site Configuration</h6>
                     </li><!-- .nk-menu-heading -->
                     <li class="nk-menu-item">
                        <a href="{{ url('/admin/page/syarat-dan-ketentuan') }}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-file-check"></em></span>
                            <span class="nk-menu-text">Syarat dan Ketentuan</span>
                        </a>
                    </li>
                    <li class="nk-menu-item">
                        <a href="{{ url('/admin/page/tentang-website') }}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-article"></em></span>
                            <span class="nk-menu-text">Tentang Website</span>
                        </a>
                    </li>
                     <li class="nk-menu-item">
                        <a href="{{ route('role.index') }}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-account-setting-alt"></em></span>
                            <span class="nk-menu-text">Role Management</span>
                        </a>
                    </li>
                     <li class="nk-menu-item">
                        <a href="{{ route('user.index') }}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-users"></em></span>
                            <span class="nk-menu-text">User Management</span>
                        </a>
                    </li>
                     <li class="nk-menu-item">
                        <a href="{{ route('contact.index') }}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-contact"></em></span>
                            <span class="nk-menu-text">Contact Us</span>
                        </a>
                    </li>
                    @endif
                 </ul><!-- .nk-menu -->
             </div><!-- .nk-sidebar-menu -->
         </div><!-- .nk-sidebar-content -->
     </div><!-- .nk-sidebar-element -->
 </div>
 <!-- sidebar @e -->
