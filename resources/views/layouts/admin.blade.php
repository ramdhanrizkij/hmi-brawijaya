@include('layouts.partials.admin.header')
<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            @include('layouts.partials.admin.sidebar')
            <!-- wrap @s -->
            <div class="nk-wrap ">
                @include('layouts.partials.admin.mainheader')

                @yield('content')
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; 2022 Main Rugby Develop By <a href="https://bytecode.id" target="_blank">Bytecode.id</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="{{asset('assets/js/bundle.js?ver=2.9.1')}}"></script>
    <script src="{{asset('assets/js/scripts.js?ver=2.9.1')}}"></script>
    <script src="{{asset('js/libs/dataTables/datatables.min.js')}}"></script>
    <script src="{{asset('js/libs/dataTables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.10/dist/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        @if ($message = Session::get('success'))
            $.toast({
                heading: 'Success',
                position: 'top-right',
                text: '{{$message}}',
                icon: 'info',
                loader: true,
                loaderBg: '#9EC600'
            });
        @endif

        @if ($message = Session::get('error'))
            $.toast({
                heading: 'Error',
                text: $message,
                icon: 'error',
                position: 'top-right'
            })
        @endif
    </script>
    @stack('script')
</body>

</html>
