@extends('layouts.admin')
@section('content')
<!-- content @s -->
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner"> 
            <div class="nk-content-body">
                <!-- Head -->
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Kategori Produk</h3>
                            <div class="nk-block-des text-soft">
                                <p>Halaman Managemen Kategori Produk</p>
                            </div>
                        </div><!-- .nk-block-head-content -->
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"
                                    data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                <div class="toggle-expand-content" data-content="pageMenu">
                                    <ul class="nk-block-tools g-3">
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>

                <!-- Head -->

                <div class="nk-block">
                    <div class="row g-gs">
                        <div class="col-lg-4">
                            <div class="card card-bordered">
                                <div class="card-inner">
                                    <div class="card-title">
                                        <h6 class="title">Tambah Kategori</h6>
                                    </div>
                                    <div>
                                        <form action="{{route('product_kategori.save')}}" method="POST" autocomplete="off"
                                            id="add-categories-form">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <input type="text" name="kategori" id="kategori" class="form-control"
                                                    placeholder="Mohon ketikkan nama kategori">
                                                <span class="text-danger error-text kategori_error"></span>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit"><i
                                                        class="icon ni ni-plus"></i>&nbsp;Create</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="card card-bordered h-100">
                                <div class="card-inner table-responsive">

                                    <table class="table table-bordered datatable" id="tableKategori">
                                        <thead>
                                            <tr>
                                                <th width="8%">No</th>
                                                <th>Nama Kategori</th>
                                                <th>Slug</th>
                                                <th>Action &nbsp;<button class="btn btn-sm btn-danger d-none"
                                                        id="deleteAllBtn">Delete All</button></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($kategori as $key=>$item)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$item->kategori}}</td>
                                                <td>{{$item->slug}}</td>
                                                <td>
                                                    <a href="#" data-id="{{$item->id}}" data-kategori="{{$item->kategori}}" id="updateKategori" class="btn btn-primary">Edit</a>
                                                    <a href="{{route('product_kategori.delete',['id'=>$item->id])}}" class="btn btn-danger" onClick="return confirm('Apakah anda yakin akan menghapus data tersebut?')">Hapus</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.product.edit-kategori')
@endsection

@push('style')
<style>
    ul.pagination {
        float: right !important;
    }

</style>
@endpush
@push('script')
<script type="text/javascript">
    $(document).on('click', '#updateKategori', function () {
            var kategori_id = $(this).data('id');
            var kategori_name =$(this).data('kategori');
            $('.editKategori').find('form')[0].reset();
            $('.editKategori').find('span.error-text').text('');
             //  alert(data.details.country_name);
             $('.editKategori').find('input[name="id"]').val(kategori_id);
            $('.editKategori').find('input[name="kategori"]').val(kategori_name);
            $('.editKategori').modal('show');
        });
    $(document).ready(function(){
        $(".datatable").DataTable();
    })
</script>
@endpush
