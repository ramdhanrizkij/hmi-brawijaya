@extends('layouts.admin')
@push('style')
<style>
    .upload-msg:hover {
        cursor: pointer
    }

    .upload-msg {
        padding-top: 80px;
        text-align: center;
        font-size: 22px;
        color: #aaa;
        margin: 10px auto;
        border: 1px solid #aaa;
    }

    .upload-photo.ready #display {
        display: block;
    }

    .upload-photo.ready .buttons #reset {
        display: inline;
    }

    .upload-photo #display,
    .upload-photo .buttons #reset,
    .upload-photo.ready .upload-msg {
        display: none;
    }

    .hide {
        display: none;
    }

</style>
@endpush
@section('content')
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Managemen Product</h3>
                            <div class="nk-block-des text-soft">
                                <p>Update Data Product</p>
                            </div>
                        </div><!-- .nk-block-head-content -->
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"
                                    data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                <div class="toggle-expand-content" data-content="pageMenu">
                                    <ul class="nk-block-tools g-3">
                                        <li><a href="{{url('admin/product')}}"
                                                class="btn btn-white btn-dim btn-outline-primary"><em
                                                    class="icon ni ni-back"></em><span>Kembali</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>

                <div class="nk-block">
                    <form method="post" action="{{route('product.save',['id'=>$product->id])}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-12">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="row mt-2">

                            <div class="col-md-8">
                                <div class="card card-bordered">
                                    <div class="card-inner">
                                        <div class="form-group">
                                            <label for=""><b>Nama Produk</b></label>
                                            <input type="text" name="nama_produk"
                                                class="form-control @error('nama_produk') is-invalid @enderror"
                                                value="{{old('nama_produk')?old('nama_produk'):$product->product_name}}">
                                            @error('nama_produk')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for=""><b>Deskripsi</b></label>
                                            <textarea name="deskripsi" id="summernote" cols="30" rows="10"
                                                class="form-control @error('deskripsi') is-invalid @enderror">{{old('deskripsi')?old('deskripsi'):$product->description}}</textarea>
                                            @error('deskripsi')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for=""><b>Kategori</b></label>
                                                <select name="kategori"
                                                    class="form-control @error('kategori') is-invalid @enderror">
                                                    <option value="">Pilih Kategori</option>
                                                    @foreach($kategori as $item)
                                                    @if(old('kategori')==$item->id || $product->category_id==$item->id)
                                                    <option value="{{$item->id}}" selected>{{$item->kategori}}
                                                    </option>
                                                    @else
                                                    <option value="{{$item->id}}">{{$item->kategori}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                                @error('kategori')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card card-bordered mt-2">
                                    <div class="card-inner">
                                        <h5>Gambar Produk</h5>
                                        @foreach($product->images as $key=>$image)
                                        <div class="product-image">
                                            <a href="{{url('admin/product/image-delete/'.$image->id)}}" onClick="return confirm('Apakah anda yakin akan menghapus gambar tersebut ?')" class="btn btn-danger btn-sm" title="Klik untuk menghapus gambar">X</a>
                                                    <img src="{{asset('uploads/product/'.$image->file)}}" />
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-bordered">
                                    <div class="card-inner">
                                        <div class="form-group">
                                            <label for="">Tambah Gambar</label>
                                            <input type="file" class="form-control" name="gambar[]" multiple  accept="image/*">
                                            <p>*)Bisa Lebih dari 1</p>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Harga</label>
                                            <input type="number" name="harga" id="" class="form-control" required
                                                value="{{old('harga')?old('harga'):$product->price}}">
                                        </div>
                                        <input type="submit" class="btn btn-primary" name="submit" value="Submit">
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
@endsection
@push('script')
<script>
    $(function () {
        $('#summernote').summernote({
            placeholder: 'Ketikkan konten berita anda disini',
            height: 300
        });


        $("#display").hide();
        $(".upload-msg").click(function () {
            $("#upload").click();
        })

        $("#upload").on('change', function (event) {
            if (event.target.files.length > 0) {
                var src = URL.createObjectURL(event.target.files[0]);
                var preview = document.getElementById("image_preview_container");
                preview.src = src;
                preview.style.display = "block";
                $(".upload-msg").hide();
                $("#display").show();
                $(".buttons").show();
            }
        })

        $("#reset").click(function () {
            var preview = document.getElementById("image_preview_container");
            preview.src = "";
            $("#display").hide();
            $(".buttons").hide();
            $(".upload-msg").show();
            $("#upload").val(null);
        });
    })

</script>
@endpush


@push('script')
<style>
    .product-image {
        padding:5px;
        border:solid #000 1px;
        width: 250px;
        position: relative;
        margin-top: 20px;
        margin-right: 20px;
        float: left;
    }

    .product-image .btn {
        position: absolute;
        right: 0px;
        top: 0px;
    }
</style>
@endpush