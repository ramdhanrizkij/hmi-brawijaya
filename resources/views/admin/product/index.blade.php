@extends('layouts.admin')
@section('content')
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <!-- Head -->
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Produk HMI</h3>
                            <div class="nk-block-des text-soft">
                                <p>Daftar Produk HMI</p>
                            </div>
                        </div><!-- .nk-block-head-content -->
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"
                                    data-target="pageMenu"><em class="icon ni ni-plus"></em></a>
                                <div class="toggle-expand-content" data-content="pageMenu">
                                    <ul class="nk-block-tools g-3">
                                        <li><a href="{{url('admin/product/create')}}"
                                                class="btn btn-white btn-dim btn-outline-primary"><em
                                                    class="icon ni ni-plus"></em><span>Tambah Produk</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>

                <!-- Head -->

                <div class="nk-block">
                    <div class="row g-gs">
                        <div class="col-lg-12">
                            <div class="card card-bordered h-100">
                                <div class="card-inner table-responsive">
                                    <table class="table table-bordered datatable"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Foto Produk</th>
                                                <th>Nama Produk</th>
                                                <th>Kategori</th>
                                                <th>Harga</th>
                                                <th>Action &nbsp;<button class="btn btn-sm btn-danger d-none"
                                                        id="deleteAllBtn">Delete All</button></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($product as $key=>$item)
                                            <tr>
                                                <td width="5%">{{$key+1}}</td>
                                                <td width="15%"><img src="{{asset('uploads/product/'.$item->thumbnail)}}" width="100px"></td>
                                                <li class="nk-menu-item">
                                    <a href="{{url('admin/product/create')}}" class="nk-menu-link"><span
                                            class="nk-menu-text">Buat Product</span></a>
                                </li> <td width="30%">{{$item->product_name}}</td>
                                                <td width="20%">{{$item->kategori?$item->kategori->kategori:'-'}}</td>
                                                <td width="10%">{{number_format($item->price,0,'.',',')}}</td>
                                                <td width="20%">
                                                    <a href="{{url('/admin/product/'.$item->id)}}" class="btn btn-primary">Update</a>
                                                    <a href="{{url('admin/product/delete/'.$item->id)}}" class="btn btn-danger" onClick="return confirm('Apakah anda yakin akan menghapus data product tersebut ?')">Delete</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(document).ready(function(){
        $(".datatable").DataTable();
    })
    </script>
   
@endpush
@push('style')
<style>
    ul.pagination {
        float: right !important;
    }

</style>
@endpush
