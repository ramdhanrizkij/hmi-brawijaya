<div class="modal fade editKategori" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Kategori</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <form action="<?= route('kategori.update') ?>" method="post" id="update-kategori-form">
                    {{csrf_field()}}
                     <input type="hidden" name="id">
                     <div class="form-group">
                         <label for="">Nama Kategori</label>
                         <input type="text" class="form-control" name="kategori" placeholder="Enter country name">
                         <span class="text-danger error-text kategori_error"></span>
                     </div>
                     <div class="form-group">
                         <button type="submit" class="btn btn-block btn-success">Simpan Perubahan</button>
                     </div>
                 </form>
            </div>
        </div>
    </div>
</div>