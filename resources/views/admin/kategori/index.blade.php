@extends('layouts.admin')
@section('content')
<!-- content @s -->
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner"> 
            <div class="nk-content-body">
                <!-- Head -->
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Managemen Categori</h3>
                            <div class="nk-block-des text-soft">
                                <p>Manage News Categories</p>
                            </div>
                        </div><!-- .nk-block-head-content -->
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"
                                    data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                <div class="toggle-expand-content" data-content="pageMenu">
                                    <ul class="nk-block-tools g-3">
                                        <li><a href="#" class="btn btn-white btn-dim btn-outline-primary"><em
                                                    class="icon ni ni-download-cloud"></em><span>Export</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>

                <!-- Head -->

                <div class="nk-block">
                    <div class="row g-gs">
                        <div class="col-lg-4">
                            <div class="card card-bordered">
                                <div class="card-inner">
                                    <div class="card-title">
                                        <h6 class="title">New Category</h6>
                                    </div>
                                    <div>
                                        <form action="{{route('kategori.save')}}" method="POST" autocomplete="off"
                                            id="add-categories-form">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <input type="text" name="kategori" id="kategori" class="form-control"
                                                    placeholder="Please type category name">
                                                <span class="text-danger error-text kategori_error"></span>
                                            </div>
                                            <div class="form-group">
                                                <select name="parent_id" class="form-control">
                                                    <option value="">Merupakan Parent</option>
                                                    @foreach($kategori as $item)
                                                    <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit"><i
                                                        class="icon ni ni-plus"></i>&nbsp;Create</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="card card-bordered h-100">
                                <div class="card-inner table-responsive">

                                    <table class="table table-bordered yajra-datatable" id="tableKategori">
                                        <thead>
                                            <tr>
                                                <th width="5%"><input type="checkbox"
                                                        name="main_checkbox"><label></label></th>
                                                <th width="8%">No</th>
                                                <th>Nama Kategori</th>
                                                <th>Action &nbsp;<button class="btn btn-sm btn-danger d-none"
                                                        id="deleteAllBtn">Delete All</button></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.kategori.edit-modal')
@endsection

@push('style')
<style>
    ul.pagination {
        float: right !important;
    }

</style>
@endpush
@push('script')
<script type="text/javascript">
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Insert
        $('#add-categories-form').on('submit', function (e) {
            e.preventDefault();
            var form = this;
            $.ajax({
                url: $(form).attr('action'),
                method: $(form).attr('method'),
                data: new FormData(form),
                processData: false,
                dataType: 'json',
                contentType: false,
                beforeSend: function () {
                    $(form).find('span.error-text').text('');
                },
                success: function (data) {
                    console.log(data);
                    $(form)[0].reset();
                    $('#tableKategori').DataTable().ajax.reload(null, false);
                    $.toast({
                        heading: 'Success',
                        position: 'top-right',
                        text: data.message,
                        icon: 'info',
                        loader: true,
                        loaderBg: '#9EC600'
                    })
                },
                error: function (xhr, status, error) {
                    var err = xhr.responseJSON.errors
                    $.each(err, function (prefix, val) {
                        $(form).find('span.' + prefix + '_error').text(val[0]);
                    })
                }
            });
        });

        // Update
        $(document).on('click', '#updateKategori', function () {
            var kategori_id = $(this).data('id');
            $('.editKategori').find('form')[0].reset();
            $('.editKategori').find('span.error-text').text('');
            $.get(`{{url('admin/categories/detail')}}/${kategori_id}`, function (data) {
                console.log(data);
                //  alert(data.details.country_name);
                $('.editKategori').find('input[name="id"]').val(data.id);
                $('.editKategori').find('input[name="kategori"]').val(data.nama_kategori);
                $('.editKategori').modal('show');
            }, 'json');
        });

        //UPDATE COUNTRY DETAILS
        $('#update-kategori-form').on('submit', function (e) {
            e.preventDefault();
            var form = this;
            $.ajax({
                url: $(form).attr('action'),
                method: $(form).attr('method'),
                data: new FormData(form),
                processData: false,
                dataType: 'json',
                contentType: false,
                beforeSend: function () {
                    $(form).find('span.error-text').text('');
                },
                success: function (data) {
                    $('.editKategori').modal('hide');
                    $(form)[0].reset();
                    $('#tableKategori').DataTable().ajax.reload(null, false);
                    $.toast({
                        heading: 'Success',
                        position: 'top-right',
                        text: data.message,
                        icon: 'info',
                        loader: true,
                        loaderBg: '#9EC600'
                    })
                },
                error: function (xhr, status, error) {
                    $('.editKategori').modal('hide');
                    var err = xhr.responseJSON.errors
                    $.each(err, function (prefix, val) {
                        $(form).find('span.' + prefix + '_error').text(val[0]);
                    })
                }
            });
        });

        // Delete
        $(document).on('click', '#deleteKategori', function () {
            var id_kategori = $(this).data('id');
            let fd = new FormData()
            fd.append('id', id_kategori)
            var url = '<?= route("kategori.delete") ?>';
            swal.fire({
                title: 'Are you sure?',
                html: 'Apakah anda yakin akan <b>menghapus</b> kategori tersebut',
                showCancelButton: true,
                showCloseButton: true,
                cancelButtonText: 'Cancel',
                confirmButtonText: 'Yes, Delete',
                cancelButtonColor: '#d33',
                confirmButtonColor: '#556ee6',
                width: 300,
                allowOutsideClick: false
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: fd,
                        processData: false,
                        dataType: 'json',
                        contentType: false,
                        success: function (data) {
                            $('#tableKategori').DataTable().ajax.reload(null,
                                false);
                            $.toast({
                                heading: 'Success',
                                position: 'top-right',
                                text: data.message,
                                icon: 'info',
                                loader: true,
                                loaderBg: '#9EC600'
                            })
                        },
                        error: function (xhr, status, error) {
                            $.toast({
                                heading: 'Error',
                                text: "Gagal menghapus data kategori, terjadi kesalahan dengan server",
                                icon: 'error',
                                position: 'top-right'
                            })
                        }
                    });
                }
            });
        });


        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('kategori.table') }}",
            columns: [{
                    data: 'checkbox',
                    name: 'checkbox',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'nama_kategori',
                    name: 'nama_kategori'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ]
        }).on('draw', function () {
            $('input[name="kategori_checkbox"]').each(function () {
                this.checked = false;
            });
            $('input[name="main_checkbox"]').prop('checked', false);
            $('button#deleteAllBtn').addClass('d-none');
        });
    });

    // Checkbox
    $(document).on('click', 'input[name="main_checkbox"]', function () {
        if (this.checked) {
            $('input[name="kategori_checkbox"]').each(function () {
                this.checked = true;
            });
        } else {
            $('input[name="kategori_checkbox"]').each(function () {
                this.checked = false;
            });
        }
        toggledeleteAllBtn();
    });

    $(document).on('change', 'input[name="kategori_checkbox"]', function () {

        if ($('input[name="kategori_checkbox"]').length == $('input[name="kategori_checkbox"]:checked')
            .length) {
            $('input[name="main_checkbox"]').prop('checked', true);
        } else {
            $('input[name="main_checkbox"]').prop('checked', false);
        }
        toggledeleteAllBtn();
    });


    function toggledeleteAllBtn() {
        if ($('input[name="kategori_checkbox"]:checked').length > 0) {
            $('button#deleteAllBtn').text('Delete (' + $('input[name="kategori_checkbox"]:checked').length + ')')
                .removeClass('d-none');
        } else {
            $('button#deleteAllBtn').addClass('d-none');
        }
    }

    $(document).on('click', 'button#deleteAllBtn', function () {
        var checkedData = [];
        $('input[name="kategori_checkbox"]:checked').each(function () {
            checkedData.push($(this).data('id'));
        });
        var url = '{{ route("kategori.delete_selected") }}';
        if (checkedData.length > 0) {
            swal.fire({
                title: 'Are you sure?',
                html: 'Apakah anda yakin akan mengpaus <b>(' + checkedData.length + ')</b> kategori',
                showCancelButton: true,
                showCloseButton: true,
                confirmButtonText: 'Yes, Delete',
                cancelButtonText: 'Cancel',
                confirmButtonColor: '#556ee6',
                cancelButtonColor: '#d33',
                width: 300,
                allowOutsideClick: false
            }).then(function (result) {
                if (result.value) {
                    $.post(url, {
                        kategori_ids: checkedData
                    }, function (data) {
                        $('#tableKategori').DataTable().ajax.reload(null, true);
                        $.toast({
                            heading: 'Success',
                            position: 'top-right',
                            text: data.message,
                            icon: 'info',
                            loader: true,
                            loaderBg: '#9EC600'
                        })
                    }, 'json');
                }
            })
        }
    });

</script>
@endpush
