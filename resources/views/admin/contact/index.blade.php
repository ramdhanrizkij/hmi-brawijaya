@extends('layouts.admin')
@section('content')
    <!-- content @s -->
    <div class="nk-content ">
        <div class="container-fluid">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <!-- Head -->
                    <div class="nk-block-head nk-block-head-sm">
                        <div class="nk-block-between">
                            <div class="nk-block-head-content">
                                <h3 class="nk-block-title page-title">Managemen Contact</h3>
                                <div class="nk-block-des text-soft">
                                    <p>Manage Contact</p>
                                </div>
                            </div><!-- .nk-block-head-content -->
                            <div class="nk-block-head-content">
                                <div class="toggle-wrap nk-block-tools-toggle">
                                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"
                                       data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                    <div class="toggle-expand-content" data-content="pageMenu">
                                        <ul class="nk-block-tools g-3">
                                           <!-- <li><a href="#" class="btn btn-white btn-dim btn-outline-primary"><em
                                                        class="icon ni ni-download-cloud"></em><span>Export</span></a></li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .nk-block-head-content -->
                        </div><!-- .nk-block-between -->
                    </div>

                    <!-- Head -->

                    <div class="nk-block">
                        <div class="row g-gs">
                            <div class="col-lg-12">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner table-responsive">

                                        <table class="table table-bordered yajra-datatable" id="tableContact">
                                            <thead>
                                            <tr>
                                                <th width="5%"><input type="checkbox"
                                                                      name="main_checkbox"><label></label></th>
                                                <th width="8%">No</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Website</th>
                                                <th>Pesan</th>
                                                <th>Action &nbsp;<button class="btn btn-sm btn-danger d-none"
                                                                         id="deleteAllBtn">Delete All</button></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-notification>
        <x-slot name="button">
            <a href="" class="btn btn-danger btn-modal-delete"><em class="mr-1 icon ni ni-trash"></em> Delete</a>
        </x-slot>
        <span id="slot"></span>
    </x-notification>
@endsection

@push('script')
    @if ($errors->any())
        <script>
            $('#inputModal').modal('show');
        </script>
    @endif
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // Delete
            $(document).on('click', '#deleteContact', function () {
                var id = $(this).data('id');
                let fd = new FormData()
                fd.append('id', id)
                var url = '<?= route("contact.destroy") ?>';
                swal.fire({
                    title: 'Are you sure?',
                    html: 'Apakah anda yakin akan <b>menghapus</b> Contact tersebut',
                    showCancelButton: true,
                    showCloseButton: true,
                    cancelButtonText: 'Cancel',
                    confirmButtonText: 'Yes, Delete',
                    cancelButtonColor: '#d33',
                    confirmButtonColor: '#556ee6',
                    width: 300,
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: url,
                            method: 'POST',
                            data: fd,
                            processData: false,
                            dataType: 'json',
                            contentType: false,
                            success: function (data) {
                                $('#tableContact').DataTable().ajax.reload(null,
                                    false);
                                $.toast({
                                    heading: 'Success',
                                    position: 'top-right',
                                    text: data.message,
                                    icon: 'info',
                                    loader: true,
                                    loaderBg: '#9EC600'
                                })
                            },
                            error: function (xhr, status, error) {
                                $.toast({
                                    heading: 'Error',
                                    text: "Gagal menghapus data Contact, terjadi kesalahan dengan server",
                                    icon: 'error',
                                    position: 'top-right'
                                })
                            }
                        });
                    }
                });
            });

            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('contact.table') }}",
                columns: [{
                    data: 'checkbox',
                    name: 'checkbox',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'website',
                        name: 'website'
                    },
                    {
                        data: 'pesan',
                        name: 'pesan'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true,
                        width: '11%'
                    },
                ]
            }).on('draw', function () {
                $('input[name="contact_checkbox"]').each(function () {
                    this.checked = false;
                });
                $('input[name="main_checkbox"]').prop('checked', false);
                $('button#deleteAllBtn').addClass('d-none');
            });
        })

        // Checkbox
        $(document).on('click', 'input[name="main_checkbox"]', function () {
            if (this.checked) {
                $('input[name="contact_checkbox"]').each(function () {
                    this.checked = true;
                });
            } else {
                $('input[name="contact_checkbox"]').each(function () {
                    this.checked = false;
                });
            }
            toggledeleteAllBtn();
        });

        $(document).on('change', 'input[name="contact_checkbox"]', function () {

            if ($('input[name="contact_checkbox"]').length == $('input[name="contact_checkbox"]:checked')
                .length) {
                $('input[name="main_checkbox"]').prop('checked', true);
            } else {
                $('input[name="main_checkbox"]').prop('checked', false);
            }
            toggledeleteAllBtn();
        });


        function toggledeleteAllBtn() {
            if ($('input[name="contact_checkbox"]:checked').length > 0) {
                $('button#deleteAllBtn').text('Delete (' + $('input[name="contact_checkbox"]:checked').length + ')')
                    .removeClass('d-none');
            } else {
                $('button#deleteAllBtn').addClass('d-none');
            }
        }

        $(document).on('click', 'button#deleteAllBtn', function () {
            var checkedData = [];
            $('input[name="contact_checkbox"]:checked').each(function () {
                checkedData.push($(this).data('id'));
            });
            var url = '{{ route("contact.delete_selected") }}';
            if (checkedData.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    html: 'Apakah anda yakin akan mengpaus <b>(' + checkedData.length + ')</b> Contact',
                    showCancelButton: true,
                    showCloseButton: true,
                    confirmButtonText: 'Yes, Delete',
                    cancelButtonText: 'Cancel',
                    confirmButtonColor: '#556ee6',
                    cancelButtonColor: '#d33',
                    width: 300,
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        $.post(url, {
                            contact_ids: checkedData
                        }, function (data) {
                            $('#tableContact').DataTable().ajax.reload(null, true);
                            $.toast({
                                heading: 'Success',
                                position: 'top-right',
                                text: data.message,
                                icon: 'info',
                                loader: true,
                                loaderBg: '#9EC600'
                            })
                        }, 'json');
                    }
                })
            }
        });

        function showModal(id, name){
            let url = "{{ url('admin/contact') }}/"+id
            $('#slot').html(name)
            $('.btn-modal-delete').attr('href', url);
            $('#notifModal').modal('show')
        }
    </script>
@endpush
