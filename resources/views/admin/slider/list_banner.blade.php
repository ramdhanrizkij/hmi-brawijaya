@extends('layouts.admin')
@section('content')
    <!-- content @s -->
    <div class="nk-content ">
        <div class="container-fluid">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="card">
                                <div class="card-header bg-primary-dim">
                                    <div class="d-flex flex-row align-middle">
                                        <h5 class="card-title mr-auto my-auto">Daftar Slider</h5>
                                        <button class="btn btn-gray" onclick="showForm()"> <em class="icon ni ni-plus"></em> New Slider </button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    @if( session()->has('success'))
                                        <div class="alert alert-success alert-dismissible fade show">
                                            {{ session('success') }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    @endif
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th width="2%">No</th>
                                                <th>Judul</th>
                                                <th>link</th>
                                                <th>Slider Image</th>
                                                <th class="text-center" width="10%">#</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php $no = 1 @endphp
                                            @foreach($datas as $row)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>{{ $row->judul }}</td>
                                                    <td>{{ $row->link }}</td>
                                                    <td><img src="{{ url('')."/storage/banner/".$row->banner_img }}" height="100rem" /></td>
                                                    <td>
                                                        <a onclick="showForm({{ $row }})" class="btn btn-primary text-white"> <em class="icon ni ni-edit"></em> Update</a>
                                                        <a href="#" class="btn btn-danger" onclick="showModal('{{ $row->id }}', '{{ $row->judul }}')" ><em class="icon ni ni-trash"></em> Delete</a>
                                                    </td>
                                                </tr>
                                                @php $no++ @endphp
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-modal>
        <x-slot name="title">Form Banner</x-slot>
        <form id="modal-form" method="POST" action="{{ route('slider.save') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" />

            <div class="form-group row {{ $errors->has('judul') ? 'has-error' : '' }}">
                <label for="judul" class="col-sm-2 col-form-label">Judul</label>
                <div class="col-sm-10">
                    <input type="text" name="judul" class="form-control" value="" />
                    @if ($errors->has('judul'))
                        <span class="text-danger">{{ $errors->first('judul') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('link') ? 'has-error' : '' }}">
                <label for="link" class="col-sm-2 col-form-label">Link</label>
                <div class="col-sm-10">
                    <input type="text" name="link" class="form-control" value="" />
                    @if ($errors->has('link'))
                        <span class="text-danger">{{ $errors->first('link') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('banner_img') ? 'has-error' : '' }}">
                <label for="banner_img" class="col-sm-2 col-form-label">Banner Image</label>
                <div class="col-sm-10">
                    <input type="file" name="banner_img" class="form-control-file" />
                    @if ($errors->has('banner_img'))
                        <span class="text-danger">{{ $errors->first('banner_img') }}</span>
                    @endif
                </div>
            </div>
            <input type="submit" class="d-none">
        </form>
        <x-slot name="button">
            <button onclick="$('form').submit()" class="btn btn-success"> <em class="icon ni ni-save"></em> Save </button>
        </x-slot>
    </x-modal>
    <x-notification>
        <x-slot name="button">
            <a href="" class="btn btn-danger btn-modal-delete"><em class="mr-1 icon ni ni-trash"></em> Delete</a>
        </x-slot>
        <span id="slot"></span>
    </x-notification>
@endsection

@push('script')
    @if ($errors->any())
        <script>
            $('#inputModal').modal('show');
        </script>
    @endif
    <script>
        $('.table').dataTable();

        function showForm(data){
            $('#modal-form')[0].reset();
            if(data != null && data != undefined) {
                $('input[name="id"]').val(data.id);
                $('input[name="judul"]').val(data.judul);
                $('input[name="link"]').val(data.link);
            }
            $('#inputModal').modal('show');
        }

        function showModal(id, name){
            let url = "{{ url('admin/slider') }}/"+id
            $('#slot').html(name)
            $('.btn-modal-delete').attr('href', url);
            $('#notifModal').modal('show')
        }
    </script>
@endpush
