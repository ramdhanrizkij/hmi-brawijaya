@extends('layouts.admin')
@push('style')
<style>
    .upload-msg:hover {
        cursor: pointer
    }

    .upload-msg {
        padding-top: 80px;
        text-align: center;
        font-size: 22px;
        color: #aaa;
        margin: 10px auto;
        border: 1px solid #aaa;
    }

    .upload-photo.ready #display {
        display: block;
    }

    .upload-photo.ready .buttons #reset {
        display: inline;
    }

    .upload-photo #display,
    .upload-photo .buttons #reset,
    .upload-photo.ready .upload-msg {
        display: none;
    }

    .hide {
        display: none;
    }

</style>
@endpush
@section('content')
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Managemen Halaman</h3>
                            <div class="nk-block-des text-soft">
                                <p>Update Halaman</p>
                            </div>
                        </div><!-- .nk-block-head-content -->
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"
                                    data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                <div class="toggle-expand-content" data-content="pageMenu">
                                    <ul class="nk-block-tools g-3">
                                        <li><a href="#" class="btn btn-white btn-dim btn-outline-primary"><em
                                                    class="icon ni ni-download-cloud"></em><span>Kembali</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>

                <div class="nk-block">
                    <form method="post" action="{{route('page.update',['id'=>$page->id])}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">

                            <div class="col-md-8">
                                <div class="card card-bordered">
                                    <div class="card-inner">
                                        <div class="form-group">
                                            <label for=""><b>Judul Halaman</b></label>
                                            <input type="text" name="judul"
                                                class="form-control @error('judul') is-invalid @enderror"
                                                value="{{old('judul')?old('judul'):$page->judul}}">
                                            @error('judul')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for=""><b>Content</b></label>
                                            <textarea name="content" id="summernote" cols="30" rows="10"
                                                class="form-control @error('content') is-invalid @enderror">{{old('content')?old('content'):$page->content}}</textarea>
                                            @error('content')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-bordered">
                                    <div class="card-inner">
                                        <div class="form-group">
                                            <input id="upload" type="file" name="thumbnail" value="Choose a file" 
                                                accept="image/*" data-role="none" hidden="">
                                            <label for=""><b>Featured Image</b></label>
                                            <div class="upload-msg" style="width:100%;height:200px;display:none">Click to upload
                                                image
                                            </div>
                                            <div id="display">
                                                <img id="image_preview_container" src="{{url('uploads/page/'.$page->thumbnail)}}" name="image"
                                                    alt="preview image" style="width: 100%;">
                                            </div>
                                            <div class="buttons text-center mt-3">
                                                <button id="reset" type="button" class="reset btn btn-danger">Change
                                                    Image</button>
                                            </div>
                                        </div>
                                        @error('thumbnail')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        <div class="form-group">
                                            <label for=""><b>Meta Title</b></label>
                                            <input type="text" name="meta_title" id=""
                                                class="form-control @error('meta_title') is-invalid @enderror" required
                                                value="{{old('meta_title')?old('meta_title'):$page->meta_title}}">
                                            @error('meta_title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for=""><b>Meta Keywords</b></label>
                                            <input type="text" name="meta_keywords" id=""
                                                class="form-control @error('meta_keywords') is-invalid @enderror"
                                                required placeholder="Pisahkan dengan ,"
                                                value="{{old('meta_keywords')?old('meta_keywords'):$page->meta_keywords}}">
                                            @error('meta_keywords')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for=""><b>Meta Description</b></label>
                                            <textarea name="meta_description" id=""
                                                class="form-control @error('meta_description') is-invalid @enderror"
                                                required>{{old('meta_descriptiption')?old('meta_description'):$page->meta_description}}</textarea>
                                            @error('meta_description')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="card card-borderd">
                                    <div class="card-inner">
                                        <input type="submit" class="btn btn-primary" name="submit" value="Save Post">
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
@endsection
@push('script')
<script>
    $(function () {
        $('#summernote').summernote({
            placeholder: 'Ketikkan konten berita anda disini',
            height: 300
        });
        $(".upload-msg").click(function () {
            $("#upload").click();
        })

        $("#upload").on('change', function (event) {
            if (event.target.files.length > 0) {
                var src = URL.createObjectURL(event.target.files[0]);
                var preview = document.getElementById("image_preview_container");
                preview.src = src;
                preview.style.display = "block";
                $(".upload-msg").hide();
                $("#display").show();
                $(".buttons").show();
            }
        })

        $("#reset").click(function () {
            var preview = document.getElementById("image_preview_container");
            preview.src = "";
            $("#display").hide();
            $(".buttons").hide();
            $(".upload-msg").show();
            $("#upload").val(null);
        });
    })

</script>
@endpush
