@extends('layouts.admin')
@section('content')
    <!-- content @s -->
    <div class="nk-content ">
        <div class="con tainer-fluid">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <!-- Head -->
                    <div class="nk-block-head nk-block-head-sm">
                        <div class="nk-block-between">
                            <div class="nk-block-head-content">
                                <h3 class="nk-block-title page-title">Profile</h3>
                                <div class="nk-block-des text-soft">
                                    <p>Update Profile</p>
                                </div>
                            </div><!-- .nk-block-head-content -->
                        </div><!-- .nk-block-between -->
                    </div>

                    <!-- Head -->

                    <div class="nk-block">
                        <div class="row g-gs">
                            <div class="col-lg-3">
                                <div class="card card-bordered">
                                    <div class="card-inner justify-content-center">
                                        <div class="border-bottom text-center">
                                            <div class="mb-3 mx-auto">
                                                <img class="rounded-circle" src="{{ url('') }}/storage/user/{{ \Auth::user()->photo_url }}" width="110px"/>
                                            </div>
                                            <h4 class="mb-0">{{ \Auth::user()->name }}</h4>
                                            <span class="text-muted d-block mb-2">{{ \Auth::user()->jabatan }}</span>
                                        </div>
                                        <div class="p-4">
                                            <strong class="text-muted d-block mb-2">Description</strong>
                                            <span>{{ \Auth::user()->profile_desc }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-9">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner">
                                        @if( session()->has('success'))
                                        <div class="alert alert-success alert-dismissible fade show">
                                            {{ session('success') }}
                                            <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close"></button>
                                        </div>
                                        @endif
                                        <div class="nk-block-head nk-block-head-lg">
                                            <div class="nk-block-between">
                                                <div class="nk-block-head-content">
                                                    <h4 class="nk-block-title">Informasi Pribadi</h4>
                                                    <div class="nk-block-des">
                                                        <p>Info dasar, seperti nama dan alamat Anda, yang Anda gunakan, di Main Rugby</p>
                                                    </div>
                                                </div>
                                                <div class="nk-block-head-content align-self-start d-lg-none">
                                                    <a href="#" class="toggle btn btn-icon btn-trigger mt-n1"
                                                       data-target="userAside"><em class="icon ni ni-menu-alt-r"></em></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="nk-block">
                                            <div class="nk-data data-list">
                                                <div class="data-head">
                                                    <h6 class="overline-title">Basics</h6>
                                                </div>
                                                <div class="data-item" data-toggle="modal"
                                                     data-target="#profile-edit">
                                                    <div class="data-col">
                                                        <span class="data-label">FullName</span>
                                                        <span class="data-value">{{ \Auth::user()->name }}</span>
                                                    </div>
                                                    <div class="data-col data-col-end">
                                                        <span class="data-more"><em class="icon ni ni-forward-ios"></em></span>
                                                    </div>
                                                </div>
                                                <div class="data-item">
                                                    <div class="data-col">
                                                        <span class="data-label">Email</span>
                                                        <span class="data-value">{{ \Auth::user()->email }}</span>
                                                    </div>
                                                    <div class="data-col data-col-end">
                                                        <span class="data-more"><em class="icon ni ni-forward-ios"></em></span>
                                                    </div>
                                                </div>
                                                <div class="data-item" data-toggle="modal"
                                                     data-target="#profile-edit">
                                                    <div class="data-col">
                                                        <span class="data-label">Role</span>
                                                        <span class="data-value">{{ \Auth::user()->role->role_name }}</span>
                                                    </div>
                                                    <div class="data-col data-col-end">
                                                        <span class="data-more"><em class="icon ni ni-forward-ios"></em></span>
                                                    </div>
                                                </div>
                                                <div class="data-item" data-toggle="modal"
                                                     data-target="#profile-edit">
                                                    <div class="data-col">
                                                        <span class="data-label">Phone Number</span>
                                                        <span class="data-value text-soft">{{ \Auth::user()->no_hp }}</span>
                                                    </div>
                                                    <div class="data-col data-col-end">
                                                        <span class="data-more"><em class="icon ni ni-forward-ios"></em></span>
                                                    </div>
                                                </div>
                                                <div class="data-item" data-toggle="modal"
                                                     data-target="#profile-edit">
                                                    <div class="data-col">
                                                        <span class="data-label">Jabatan</span>
                                                        <span class="data-value">{{ \Auth::user()->jabatan }}</span>
                                                    </div>
                                                    <div class="data-col data-col-end">
                                                        <span class="data-more"><em class="icon ni ni-forward-ios"></em></span>
                                                    </div>
                                                </div>
                                                <div class="data-item" data-toggle="modal"
                                                     data-target="#profile-edit" data-tab-target="#address">
                                                    <div class="data-col">
                                                        <span class="data-label">Profile Description</span>
                                                        <span class="data-value">{{ \Auth::user()->profile_desc }}</span>
                                                    </div>
                                                    <div class="data-col data-col-end">
                                                        <span class="data-more"><em class="icon ni ni-forward-ios"></em></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="nk-block-head nk-block-head-lg">
                                            <div class="nk-block-between">
                                                <div class="nk-block-head-content">
                                                    <h4 class="nk-block-title">Keamanan Akun</h4>
                                                    <div class="nk-block-des">
                                                        <p></p>
                                                    </div>
                                                </div>
                                                <div class="nk-block-head-content align-self-start d-lg-none">
                                                    <a href="#" class="toggle btn btn-icon btn-trigger mt-n1"
                                                       data-target="userAside"><em class="icon ni ni-menu-alt-r"></em></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="nk-block">
                                            <div class="nk-data data-list">
                                                <div class="data-head">
                                                    <h6 class="overline-title">Password</h6>
                                                </div>
                                                <div class="data-item" data-toggle="modal"
                                                     data-target="#profile-edit">
                                                    <div class="data-col">
                                                        <span class="data-label">Password</span>
                                                        <span class="data-value">Change password</span>
                                                    </div>
                                                    <div class="data-col data-col-end">
                                                        <span class="change-pwd"><em class="icon ni ni-forward-ios"></em></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-modal>
        <x-slot name="title">Edit Profile</x-slot>
        <form id="modal-form" method="POST" action="{{ route('user.save') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="profile" value="profile">
            <input type="hidden" name="id" />

            <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" />
                    @if ($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}" />
                    @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('role_id') ? 'has-error' : '' }}">
                <label for="role_id" class="col-sm-2 col-form-label">Role</label>
                <div class="col-sm-10">
                    <select name="role_id" class="form-control">
                        <option selected hidden disabled value="">- Select Role -</option>
                        @foreach($roles as $row)
                            <option value="{{ $row->id }}" {{  (old('role_id') ? old('role_id') : '') == $row->id ? 'selected' : ''}}>{{ $row->role_name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('role_id'))
                        <span class="text-danger">{{ $errors->first('role_id') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('no_hp') ? 'has-error' : '' }}">
                <label for="no_hp" class="col-sm-2 col-form-label">No HP</label>
                <div class="col-sm-10">
                    <input type="text" name="no_hp" class="form-control" value="{{ old('no_hp') }}" />
                    @if ($errors->has('no_hp'))
                        <span class="text-danger">{{ $errors->first('no_hp') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('jabatan') ? 'has-error' : '' }}">
                <label for="jabatan" class="col-sm-2 col-form-label">Jabatan</label>
                <div class="col-sm-10">
                    <input type="text" name="jabatan" class="form-control" value="{{ old('jabatan') }}" />
                    @if ($errors->has('jabatan'))
                        <span class="text-danger">{{ $errors->first('jabatan') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('profile_desc') ? 'has-error' : '' }}">
                <label for="profile_desc" class="col-sm-2 col-form-label">Profile Description</label>
                <div class="col-sm-10">
                    <textarea name="profile_desc" class="form-control">{{ old('profile_desc') }}</textarea>
                    @if ($errors->has('profile_desc'))
                        <span class="text-danger">{{ $errors->first('profile_desc') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('photo_url') ? 'has-error' : '' }}">
                <label for="photo_url" class="col-sm-2 col-form-label">Photo Profile</label>
                <div class="col-sm-10">
                    <input type="file" name="photo_url" class="form-control-file" />
                    @if ($errors->has('photo_url'))
                        <span class="text-danger">{{ $errors->first('photo_url') }}</span>
                    @endif
                </div>
            </div>
            <input type="submit" class="d-none">
        </form>
        <x-slot name="button">
            <button onclick="$('#modal-form').submit()" class="btn btn-success"> <em class="icon ni ni-save"></em> Save </button>
        </x-slot>
    </x-modal>


    <x-notification>
        <x-slot name="button">
            <a href="" class="btn btn-danger btn-modal-delete"><em class="mr-1 icon ni ni-trash"></em> Delete</a>
        </x-slot>
        <span id="slot"></span>
    </x-notification>
    @include('admin.user.change_password')
@endsection

@push('script')
    @if ($errors->has('password'))
        <script>
            $('#changePwdModal').modal('show');
        </script>
    @elseif($errors->any())
        <script>
            $('#inputModal').modal('show');
        </script>
    @endif
    <script>
        function showModal(id, name){
            let url = "{{ url('admin/user/delete') }}/"+id
            $('#slot').html(name)
            $('.btn-modal-delete').attr('href', url);
            $('#notifModal').modal('show')
        }

        $(document).on('click', '.change-pwd', function (){
            $('#changePwdModal').modal('show');
        })

        // Update
        $(document).on('click', '.data-more', function () {
            var user_id = "{{ \Auth::user()->id }}";
            $('#modal-form')[0].reset();
            $.get(`{{url('admin/user/detail')}}/${user_id}`, function (data) {
                console.log(data);
                //  alert(data.details.country_name);

                $('input[name="id"]').val(data.id);
                $('input[name="name"]').val(data.name);
                $('input[name="email"]').val(data.email);
                $('input[name="no_hp"]').val(data.no_hp);
                $('input[name="jabatan"]').val(data.jabatan);
                $('textarea[name="profile_desc"]').html(data.profile_desc);
                $('select[name="role_id"]').val(data.role_id).change();
                // $('.id_100 option[value=val2]').attr('selected','selected');

                $('#inputModal').modal('show');
            }, 'json');
        });
    </script>
@endpush
