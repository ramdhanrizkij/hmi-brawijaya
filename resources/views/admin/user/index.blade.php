@extends('layouts.admin')
@section('content')
    <!-- content @s -->
    <div class="nk-content ">
        <div class="container-fluid">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <!-- Head -->
                    <div class="nk-block-head nk-block-head-sm">
                        <div class="nk-block-between">
                            <div class="nk-block-head-content">
                                <h3 class="nk-block-title page-title">Managemen User</h3>
                                <div class="nk-block-des text-soft">
                                    <p>Manage User</p>
                                </div>
                            </div><!-- .nk-block-head-content -->
                            <div class="nk-block-head-content">
                                <div class="toggle-wrap nk-block-tools-toggle">
                                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"
                                       data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                    <div class="toggle-expand-content" data-content="pageMenu">
                                        <ul class="nk-block-tools g-3">
                                            <!-- <li><a href="#" class="btn btn-white btn-dim btn-outline-primary"><em
                                                        class="icon ni ni-download-cloud"></em><span>Export</span></a></li> --> 
                                            <li><a href="#" onclick="showForm()" class="btn btn-white btn-dim btn-outline-success"><em
                                                        class="icon ni ni-plus"></em><span>New User</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .nk-block-head-content -->
                        </div><!-- .nk-block-between -->
                    </div>

                    <!-- Head -->

                    <div class="nk-block">
                        <div class="row g-gs">
                            <div class="col-lg-12">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner table-responsive">

                                        <table class="table table-bordered yajra-datatable" id="tableUser">
                                            <thead>
                                            <tr>
                                                <th width="5%"><input type="checkbox"
                                                                      name="main_checkbox"><label></label></th>
                                                <th width="8%">No</th>
                                                <th>Profile</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>No HP</th>
                                                <th>Jabatan</th>
                                                <th>Profile Description</th>
                                                <th>
                                                    Action &nbsp;
                                                    <button class="btn btn-sm btn-danger d-none" id="deleteAllBtn">Delete All</button>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-modal>
        <x-slot name="title">Form User</x-slot>
        <form id="modal-form" method="POST" action="{{ route('user.save') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" />

            <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" />
                    @if ($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}" />
                    @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('password') ? 'has-error' : '' }}">
                <label for="password" class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10">
                    <input type="password" name="password" class="form-control" value="{{ old('password') }}" />
                    @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                <label for="password_confirmation" class="col-sm-2 col-form-label">Password Confirmation</label>
                <div class="col-sm-10">
                    <input type="password" name="password_confirmation" class="form-control" value="{{ old('password_confirmation') }}" />
                    @if ($errors->has('password_confirmation'))
                        <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('role_id') ? 'has-error' : '' }}">
                <label for="role_id" class="col-sm-2 col-form-label">Role</label>
                <div class="col-sm-10">
                    <select name="role_id" class="form-control">
                        <option selected hidden disabled value="">- Select Role -</option>
                        @foreach($roles as $row)
                            <option value="{{ $row->id }}" {{  (old('role_id') ? old('role_id') : '') == $row->id ? 'selected' : ''}}>{{ $row->role_name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('role_id'))
                        <span class="text-danger">{{ $errors->first('role_id') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('no_hp') ? 'has-error' : '' }}">
                <label for="no_hp" class="col-sm-2 col-form-label">No HP</label>
                <div class="col-sm-10">
                    <input type="text" name="no_hp" class="form-control" value="{{ old('no_hp') }}" />
                    @if ($errors->has('no_hp'))
                        <span class="text-danger">{{ $errors->first('no_hp') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('jabatan') ? 'has-error' : '' }}">
                <label for="jabatan" class="col-sm-2 col-form-label">Jabatan</label>
                <div class="col-sm-10">
                    <input type="text" name="jabatan" class="form-control" value="{{ old('jabatan') }}" />
                    @if ($errors->has('jabatan'))
                        <span class="text-danger">{{ $errors->first('jabatan') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('profile_desc') ? 'has-error' : '' }}">
                <label for="profile_desc" class="col-sm-2 col-form-label">Profile Description</label>
                <div class="col-sm-10">
                    <textarea name="profile_desc" class="form-control">{{ old('profile_desc') }}</textarea>
                    @if ($errors->has('profile_desc'))
                        <span class="text-danger">{{ $errors->first('profile_desc') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('photo_url') ? 'has-error' : '' }}">
                <label for="photo_url" class="col-sm-2 col-form-label">Photo Profile</label>
                <div class="col-sm-10">
                    <input type="file" name="photo_url" class="form-control-file" />
                    @if ($errors->has('photo_url'))
                        <span class="text-danger">{{ $errors->first('photo_url') }}</span>
                    @endif
                </div>
            </div>
            <input type="submit" class="d-none">
        </form>
        <x-slot name="button">
            <button onclick="$('form').submit()" class="btn btn-success"> <em class="icon ni ni-save"></em> Save </button>
        </x-slot>
    </x-modal>
    <x-notification>
        <x-slot name="button">
            <a href="" class="btn btn-danger btn-modal-delete"><em class="mr-1 icon ni ni-trash"></em> Delete</a>
        </x-slot>
        <span id="slot"></span>
    </x-notification>
@endsection

@push('script')
    @if ($errors->any())
        <script>
            $('#inputModal').modal('show');
        </script>
    @endif
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Update
            $('#updateUser').on('click', function (e) {
                // e.preventDefault();
                console.log("here")
                var id = $(this).attr('data-id');
                $.ajax({
                    url: "{{ url('/admin/user/detail') }}/"+id,
                    method: "GET",
                    processData: false,
                    dataType: 'json',
                    contentType: false,
                    success: function (data) {
                        console.log(data);

                        $('input[name="id"]').val(data.id);
                        $('input[name="name"]').val(data.name);
                        $('input[name="link"]').val(data.link);


                        $('#inputModal').modal('show');
                    },
                    error: function (xhr, status, error) {
                        var err = xhr.responseJSON.errors
                        $.each(err, function (prefix, val) {
                            $(form).find('span.' + prefix + '_error').text(val[0]);
                        })
                    }
                });
            });

            // Delete
            $(document).on('click', '#deleteUser', function () {
                var id = $(this).data('id');
                let fd = new FormData()
                fd.append('id', id)
                var url = '<?= route("user.destroy") ?>';
                swal.fire({
                    title: 'Are you sure?',
                    html: 'Apakah anda yakin akan <b>menghapus</b> User tersebut',
                    showCancelButton: true,
                    showCloseButton: true,
                    cancelButtonText: 'Cancel',
                    confirmButtonText: 'Yes, Delete',
                    cancelButtonColor: '#d33',
                    confirmButtonColor: '#556ee6',
                    width: 300,
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: url,
                            method: 'POST',
                            data: fd,
                            processData: false,
                            dataType: 'json',
                            contentType: false,
                            success: function (data) {
                                $('#tableVideo').DataTable().ajax.reload(null,
                                    false);
                                $.toast({
                                    heading: 'Success',
                                    position: 'top-right',
                                    text: data.message,
                                    icon: 'info',
                                    loader: true,
                                    loaderBg: '#9EC600'
                                })
                            },
                            error: function (xhr, status, error) {
                                $.toast({
                                    heading: 'Error',
                                    text: "Gagal menghapus data user, terjadi kesalahan dengan server",
                                    icon: 'error',
                                    position: 'top-right'
                                })
                            }
                        });
                    }
                });
            });

            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('user.table') }}",
                columns: [{
                    data: 'checkbox',
                    name: 'checkbox',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                    {
                        data: 'photo_url',
                        name: 'photo_url'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'role.role_name',
                        name: 'role'
                    },
                    {
                        data: 'no_hp',
                        name: 'no_hp'
                    },
                    {
                        data: 'jabatan',
                        name: 'jabatan'
                    },
                    {
                        data: 'profile_desc',
                        name: 'profile_desc'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true,
                        width: '11%'
                    },
                ]
            }).on('draw', function () {
                $('input[name="user_checkbox"]').each(function () {
                    this.checked = false;
                });
                $('input[name="main_checkbox"]').prop('checked', false);
                $('button#deleteAllBtn').addClass('d-none');
            });
        })

        // Update
        $(document).on('click', '#updateUser', function () {
            var user_id = $(this).data('id');
            $('#modal-form')[0].reset();
            $.get(`{{url('admin/user/detail')}}/${user_id}`, function (data) {
                console.log(data);
                //  alert(data.details.country_name);

                $('input[name="id"]').val(data.id);
                $('input[name="name"]').val(data.name);
                $('input[name="email"]').val(data.email);
                $('input[name="no_hp"]').val(data.no_hp);
                $('input[name="jabatan"]').val(data.jabatan);
                $('textarea[name="profile_desc"]').html(data.profile_desc);
                $('select[name="role_id"]').val(data.role_id).change();
                // $('.id_100 option[value=val2]').attr('selected','selected');

                $('#inputModal').modal('show');
            }, 'json');
        });

        // Checkbox
        $(document).on('click', 'input[name="main_checkbox"]', function () {
            if (this.checked) {
                $('input[name="user_checkbox"]').each(function () {
                    this.checked = true;
                });
            } else {
                $('input[name="user_checkbox"]').each(function () {
                    this.checked = false;
                });
            }
            toggledeleteAllBtn();
        });

        $(document).on('change', 'input[name="user_checkbox"]', function () {

            if ($('input[name="user_checkbox"]').length == $('input[name="user_checkbox"]:checked')
                .length) {
                $('input[name="main_checkbox"]').prop('checked', true);
            } else {
                $('input[name="main_checkbox"]').prop('checked', false);
            }
            toggledeleteAllBtn();
        });


        function toggledeleteAllBtn() {
            if ($('input[name="user_checkbox"]:checked').length > 0) {
                $('button#deleteAllBtn').text('Delete (' + $('input[name="user_checkbox"]:checked').length + ')')
                    .removeClass('d-none');
            } else {
                $('button#deleteAllBtn').addClass('d-none');
            }
        }

        $(document).on('click', 'button#deleteAllBtn', function () {
            var checkedData = [];
            $('input[name="user_checkbox"]:checked').each(function () {
                checkedData.push($(this).data('id'));
            });
            var url = '{{ route("user.delete_selected") }}';
            if (checkedData.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    html: 'Apakah anda yakin akan mengpaus <b>(' + checkedData.length + ')</b> user',
                    showCancelButton: true,
                    showCloseButton: true,
                    confirmButtonText: 'Yes, Delete',
                    cancelButtonText: 'Cancel',
                    confirmButtonColor: '#556ee6',
                    cancelButtonColor: '#d33',
                    width: 300,
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        $.post(url, {
                            user_ids: checkedData
                        }, function (data) {
                            $('#tableUser').DataTable().ajax.reload(null, true);
                            $.toast({
                                heading: 'Success',
                                position: 'top-right',
                                text: data.message,
                                icon: 'info',
                                loader: true,
                                loaderBg: '#9EC600'
                            })
                        }, 'json');
                    }
                })
            }
        });

        function showForm(data){
            $('#modal-form')[0].reset();
            $('#inputModal').modal('show');
        }

        function showModal(id, name){
            let url = "{{ url('admin/user/delete') }}/"+id
            $('#slot').html(name)
            $('.btn-modal-delete').attr('href', url);
            $('#notifModal').modal('show')
        }
    </script>
@endpush
