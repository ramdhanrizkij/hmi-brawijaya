@extends('layouts.admin')
@section('content')
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <!-- Head -->
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Managemen Banner</h3>
                            <div class="nk-block-des text-soft">
                                <p>Klik salah satu kotak untuk mengubah banner</p>
                            </div>
                        </div><!-- .nk-block-head-content -->
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-plus"></em></a>
                                <div class="toggle-expand-content" data-content="pageMenu">

                                </div>
                            </div>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>

                <!-- Head -->

                <div class="nk-block">
                    <div class="row g-gs">
                        <div class="col-lg-12">
                            <div class="card card-bordered h-100">
                                <div class="card-inner table-responsive">
                                    <div class="homepage" style="display:flex">
                                        <div class="homepage-banner banner" 
                                            style="flex:1" 
                                            onClick="updateBanner('homepage',1,'{{$homepage1->link_url??'#'}}')">
                                            @if($homepage1)
                                                <img src="{{asset('uploads/banner_iklan/'.$homepage1->banner_image)}}"> 
                                            @else 
                                                <h2>1920 x 510</h2>
                                            @endif
                                        </div>
                                        <div class="homepage-banner banner" 
                                            style="flex:1" 
                                            onClick="updateBanner('homepage',2,'{{$homepage2->link_url??'#'}}')">
                                            @if($homepage2)
                                                <img src="{{asset('uploads/banner_iklan/'.$homepage2->banner_image)}}"> 
                                            @else 
                                                <h2>1920 x 510</h2>
                                            @endif
                                        </div>
                                        <div class="homepage-banner banner" 
                                            style="flex:1" 
                                            onClick="updateBanner('homepage',3,'{{$homepage3->link_url??'#'}}')">
                                            @if($homepage3)
                                                <img src="{{asset('uploads/banner_iklan/'.$homepage3->banner_image)}}"> 
                                            @else 
                                                <h2>1920 x 510</h2>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="content">
                                            <div style="flex:1;text-align:center;">
                                               <h4 style="margin-top:50px;">Konten Berita</h4>
                                            </div>
                                            <div class="banner sidebar-baner" onClick="updateBanner('sidebar',1,'{{$sidebar->link_url??'#'}}')">
                                            @if($sidebar)
                                                <img src="{{asset('uploads/banner_iklan/'.$sidebar->banner_image)}}"> 
                                            @else 
                                                <h2>366 x 280</h2>
                                            @endif
                                        </div>
                                    </div>

                                    <div style="display:flex;justify-content:center;">
                                        <div class="banner bottom"onClick="updateBanner('bottom',1,'{{$bottom->link_url??'#'}}')">
                                            @if($bottom)
                                                <img src="{{asset('uploads/banner_iklan/'.$bottom->banner_image)}}"> 
                                            @else 
                                                <h2>728 x 90</h2>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade updateBanner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Banner</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= route('banner.update') ?>" method="post"  enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="posisi">
                    <input type="hidden" name="kategori">
                    <div class="form-group">
                        <label for="">Link Url</label>
                        <input type="text" class="form-control" name="link" placeholder="Mohon masukkan link url" required>
                        <span class="text-danger error-text"></span>
                    </div>
                    <div class="form-group">
                        <label for="">Gambar Banner</label>
                        <input type="file" class="form-control" name="banner" placeholder="">
                        <span class="text-danger error-text"></span>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-success">Simpan Perubahan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 

@endsection

@push('script')
<script>
    function updateBanner(kategori, posisi,link) {
        $('.updateBanner').find('input[name="posisi"]').val(posisi);
        $('.updateBanner').find('input[name="kategori"]').val(kategori);
        $('.updateBanner').find('input[name="link"]').val(link);
        $(".updateBanner").modal('show')
    }
</script>
@endpush
@push('style')
<style>

    .banner {
        cursor: pointer;
        display: flex;
        justify-content: center;
        align-items: center;
        background: #A9A9A9;
    }
    .homepage-banner {
        height: 200px;
        margin-right: 15px;
    }

    .content {
        margin-top:15px;
        display:flex;
        justify-content:flex-end
    }
    .sidebar-baner {
        height:280px;
        width:366px;
        margin-right:15px
    }

    .bottom {
        height:90px;
        width:728px;
    }
    .banner h2 {
        color: #898989 !important;
    }

    .banner img {
        position: relative;
        width:100%;
        height:100%;
        object-fit:cover
    }
</style>
@endpush