@extends('layouts.admin')
@section('content')
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <!-- Head -->
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Managemen Post</h3>
                            <div class="nk-block-des text-soft">
                                <p>Daftar Post</p>
                            </div>
                        </div><!-- .nk-block-head-content -->
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"
                                    data-target="pageMenu"><em class="icon ni ni-plus"></em></a>
                                <div class="toggle-expand-content" data-content="pageMenu">
                                    <ul class="nk-block-tools g-3">
                                        <li><a href="{{url('admin/berita/create')}}"
                                                class="btn btn-white btn-dim btn-outline-primary"><em
                                                    class="icon ni ni-plus"></em><span>Create New</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>

                <!-- Head -->

                <div class="nk-block">
                    <div class="row g-gs">
                        <div class="col-lg-12">
                            <div class="card card-bordered h-100">
                                <div class="card-inner table-responsive">
                                    <table class="table table-bordered yajra-datatable" id="tableKategori"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="5%"><input type="checkbox"
                                                        name="main_checkbox"><label></label></th>
                                                <th width="5%">No</th>
                                                <th>Judul</th>
                                                <th>Kategori</th>
                                                <th>Tags</th>
                                                @if(Auth::user()->role_id!=3)
                                                <th>Penulis</th>
                                                @endif
                                                <th>Created At</th>
                                                <th>Status</th>
                                                <th>Action &nbsp;<button class="btn btn-sm btn-danger d-none"
                                                        id="deleteAllBtn">Delete All</button></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('berita.table') }}",
            columns: [{
                    data: 'checkbox',
                    name: 'checkbox',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'judul',
                    name: 'judul'
                },
                {
                    data: 'kategori.nama_kategori',
                    name: 'kategori'
                },
                {
                    data: 'tags',
                    name: 'tags',
                    orderable: false,
                    searchable: false
                },
                @if(Auth::user()->role_id!=3)
                {
                    data: 'user.name',
                    name: 'user',
                    orderable: false,
                    searchable: false
                },
                @endif
                {
                    data: 'created_at',
                    name: 'created_at',
                },
                {
                    data: 'status',
                    name: 'status',
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ]
        }).on('draw', function () {
            $('input[name="selected_checkbox"]').each(function () {
                this.checked = false;
            });
            $('input[name="main_checkbox"]').prop('checked', false);
            $('button#deleteAllBtn').addClass('d-none');
        });

        // Checkbox
        $(document).on('click', 'input[name="main_checkbox"]', function () {
            if (this.checked) {
                $('input[name="selected_checkbox"]').each(function () {
                    this.checked = true;
                });
            } else {
                $('input[name="selected_checkbox"]').each(function () {
                    this.checked = false;
                });
            }
            toggledeleteAllBtn();
        });

        $(document).on('change', 'input[name="selected_checkbox"]', function () {

            if ($('input[name="selected_checkbox"]').length == $(
                    'input[name="selected_checkbox"]:checked')
                .length) {
                $('input[name="main_checkbox"]').prop('checked', true);
            } else {
                $('input[name="main_checkbox"]').prop('checked', false);
            }
            toggledeleteAllBtn();
        });


        function toggledeleteAllBtn() {
            if ($('input[name="selected_checkbox"]:checked').length > 0) {
                $('button#deleteAllBtn').text('Delete (' + $('input[name="selected_checkbox"]:checked').length +
                        ')')
                    .removeClass('d-none');
            } else {
                $('button#deleteAllBtn').addClass('d-none');
            }
        }

        $(document).on('click', 'button#deleteAllBtn', function () {
            var checkedData = [];
            $('input[name="selected_checkbox"]:checked').each(function () {
                checkedData.push($(this).data('id'));
            });
            var url = '{{ route("berita.bulk_delete") }}';
            if (checkedData.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    html: 'Apakah anda yakin akan mengpaus <b>(' + checkedData.length +
                        ')</b> kategori',
                    showCancelButton: true,
                    showCloseButton: true,
                    confirmButtonText: 'Yes, Delete',
                    cancelButtonText: 'Cancel',
                    confirmButtonColor: '#556ee6',
                    cancelButtonColor: '#d33',
                    width: 300,
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        $.post(url, {
                            post_ids: checkedData
                        }, function (data) {
                            $('#tableKategori').DataTable().ajax.reload(null, true);
                            $.toast({
                                heading: 'Success',
                                position: 'top-right',
                                text: data.message,
                                icon: 'info',
                                loader: true,
                                loaderBg: '#9EC600'
                            })
                        }, 'json');
                    }
                })
            }
        });
    });

</script>

@endpush
@push('style')
<style>
    ul.pagination {
        float: right !important;
    }

</style>
@endpush
