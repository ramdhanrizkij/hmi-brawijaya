@extends('layouts.admin')
@section('content')
    <!-- content @s -->
    <div class="nk-content ">
        <div class="container-fluid">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="card">
                                <div class="card-header bg-primary-dim">
                                    <div class="d-flex flex-row align-middle">
                                        <h5 class="card-title mr-auto my-auto">Daftar Video</h5>
                                        <button class="btn btn-gray" onclick="showForm()"> <em class="icon ni ni-plus"></em> New Video </button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    @if( session()->has('success'))
                                        <div class="alert alert-success alert-dismissible fade show">
                                            {{ session('success') }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    @endif
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th width="2%">No</th>
                                                <th>Judul</th>
                                                <th>Video_url</th>
                                                <th>Description</th>
                                                <th class="text-center" width="10%">#</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php $no = 1 @endphp
                                            @foreach($datas as $row)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>{{ $row->judul }}</td>
                                                    <td>
                                                        <a href="{{ $row->video_url }}" target="_blank">{{ $row->video_url }}</a>
                                                    </td>
                                                    <td>{{ $row->description }}</td>
                                                    <td>
                                                        <a class="btn btn-primary text-white" onclick="showForm({{ $row }})"> <em class="icon ni ni-edit"></em></a>
                                                        <a class="btn btn-danger" href="#" onclick="showModal('{{ $row->id }}', '{{ $row->judul }}')" ><em class="icon ni ni-trash"></em> </a>
                                                    </td>
                                                </tr>
                                                @php $no++ @endphp
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-modal>
        <x-slot name="title">Form Video</x-slot>
        <form method="POST" action="{{ route('video.save') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" />

            <div class="form-group row {{ $errors->has('judul') ? 'has-error' : '' }}">
                <label for="judul" class="col-sm-2 col-form-label">Judul</label>
                <div class="col-sm-10">
                    <input type="text" name="judul" class="form-control" value="{{ old('judul') }}" />
                    @if ($errors->has('judul'))
                        <span class="text-danger">{{ $errors->first('judul') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('video_url') ? 'has-error' : '' }}">
                <label for="video_url" class="col-sm-2 col-form-label">Video URL</label>
                <div class="col-sm-10">
                    <input type="text" name="video_url" class="form-control" value="{{ old('video_url') }}" />
                    @if ($errors->has('video_url'))
                        <span class="text-danger">{{ $errors->first('video_url') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description" class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                    <textarea name="description" class="form-control">{{ old('description') }}</textarea>
                    @if ($errors->has('description'))
                        <span class="text-danger">{{ $errors->first('description') }}</span>
                    @endif
                </div>
            </div>
            <input type="submit" class="d-none">
        </form>
        <x-slot name="button">
            <button onclick="$('form').submit()" class="btn btn-success"> <em class="icon ni ni-save"></em> Save </button>
        </x-slot>
    </x-modal>
    <x-notification>
        <x-slot name="button">
            <a href="" class="btn btn-danger btn-modal-delete"><em class="mr-1 icon ni ni-trash"></em> Delete</a>
        </x-slot>
        <span id="slot"></span>
    </x-notification>
@endsection

@push('script')
    @if ($errors->any())
        <script>
            $('#inputModal').modal('show');
        </script>
    @endif
    <script>
        $('.table').dataTable();

        function showForm(data){
            if(data != null && data != undefined) {
                $('input[name="id"]').val(data.id);
                $('input[name="judul"]').val(data.judul);
                $('input[name="video_url"]').val(data.video_url);
                $('textarea[name="description"]').html(data.description);
            }
            $('#inputModal').modal('show');
        }

        function showModal(id, name){
            let url = "{{ url('admin/video') }}/"+id
            $('#slot').html(name)
            $('.btn-modal-delete').attr('href', url);
            $('#notifModal').modal('show')
        }
    </script>
@endpush
