@extends('layouts.admin')
@section('content')
    <!-- content @s -->
    <div class="nk-content ">
        <div class="container-fluid">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <!-- Head -->
                    <div class="nk-block-head nk-block-head-sm">
                        <div class="nk-block-between">
                            <div class="nk-block-head-content">
                                <h3 class="nk-block-title page-title">Managemen Video</h3>
                                <div class="nk-block-des text-soft">
                                    <p>Manage Video</p>
                                </div>
                            </div><!-- .nk-block-head-content -->
                            <div class="nk-block-head-content">
                                <div class="toggle-wrap nk-block-tools-toggle">
                                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"
                                       data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                    <div class="toggle-expand-content" data-content="pageMenu">
                                        <ul class="nk-block-tools g-3">
                                           <!-- <li><a href="#" class="btn btn-white btn-dim btn-outline-primary"><em
                                                        class="icon ni ni-download-cloud"></em><span>Export</span></a></li> -->
                                            <li><a href="#" onclick="showForm()" class="btn btn-white btn-dim btn-outline-success"><em
                                                        class="icon ni ni-plus"></em><span>New Video</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .nk-block-head-content -->
                        </div><!-- .nk-block-between -->
                    </div>

                    <!-- Head -->

                    <div class="nk-block">
                        <div class="row g-gs">
                            <div class="col-lg-12">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner table-responsive">

                                        <table class="table table-bordered yajra-datatable" id="tableVideo">
                                            <thead>
                                            <tr>
                                                <th width="5%"><input type="checkbox"
                                                                      name="main_checkbox"><label></label></th>
                                                <th width="8%">No</th>
                                                <th>Judul</th>
                                                <th>Video</th>
                                                <th>Description</th>
                                                <th>Action &nbsp;<button class="btn btn-sm btn-danger d-none"
                                                                         id="deleteAllBtn">Delete All</button></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-modal>
        <x-slot name="title">Form Video</x-slot>
        <form method="POST" action="{{ route('video.save') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" />

            <div class="form-group row {{ $errors->has('judul') ? 'has-error' : '' }}">
                <label for="judul" class="col-sm-2 col-form-label">Judul</label>
                <div class="col-sm-10">
                    <input type="text" name="judul" class="form-control" value="{{ old('judul') }}" />
                    @if ($errors->has('judul'))
                        <span class="text-danger">{{ $errors->first('judul') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('video_url') ? 'has-error' : '' }}">
                <label for="video_url" class="col-sm-2 col-form-label">Video URL</label>
                <div class="col-sm-10">
                    <input type="text" name="video_url" class="form-control" value="{{ old('video_url') }}" />
                    @if ($errors->has('video_url'))
                        <span class="text-danger">{{ $errors->first('video_url') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description" class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                    <textarea name="description" class="form-control">{{ old('description') }}</textarea>
                    @if ($errors->has('description'))
                        <span class="text-danger">{{ $errors->first('description') }}</span>
                    @endif
                </div>
            </div>
            <input type="submit" class="d-none">
        </form>
        <x-slot name="button">
            <button onclick="$('form').submit()" class="btn btn-success"> <em class="icon ni ni-save"></em> Save </button>
        </x-slot>
    </x-modal>
    <x-notification>
        <x-slot name="button">
            <a href="" class="btn btn-danger btn-modal-delete"><em class="mr-1 icon ni ni-trash"></em> Delete</a>
        </x-slot>
        <span id="slot"></span>
    </x-notification>
@endsection

@push('script')
    @if ($errors->any())
        <script>
            $('#inputModal').modal('show');
        </script>
    @endif
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // Delete
            $(document).on('click', '#deleteVideo', function () {
                var id = $(this).data('id');
                let fd = new FormData()
                fd.append('id', id)
                var url = '<?= route("video.destroy") ?>';
                swal.fire({
                    title: 'Are you sure?',
                    html: 'Apakah anda yakin akan <b>menghapus</b> Video tersebut',
                    showCancelButton: true,
                    showCloseButton: true,
                    cancelButtonText: 'Cancel',
                    confirmButtonText: 'Yes, Delete',
                    cancelButtonColor: '#d33',
                    confirmButtonColor: '#556ee6',
                    width: 300,
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: url,
                            method: 'POST',
                            data: fd,
                            processData: false,
                            dataType: 'json',
                            contentType: false,
                            success: function (data) {
                                $('#tableVideo').DataTable().ajax.reload(null,
                                    false);
                                $.toast({
                                    heading: 'Success',
                                    position: 'top-right',
                                    text: data.message,
                                    icon: 'info',
                                    loader: true,
                                    loaderBg: '#9EC600'
                                })
                            },
                            error: function (xhr, status, error) {
                                $.toast({
                                    heading: 'Error',
                                    text: "Gagal menghapus data video, terjadi kesalahan dengan server",
                                    icon: 'error',
                                    position: 'top-right'
                                })
                            }
                        });
                    }
                });
            });

            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('video.table') }}",
                columns: [{
                    data: 'checkbox',
                    name: 'checkbox',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                    {
                        data: 'judul',
                        name: 'judul'
                    },
                    {
                        data: 'video_url',
                        name: 'video_url'
                    },
                    {
                        data: 'description',
                        name: 'description'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true,
                        width: '11%'
                    },
                ]
            }).on('draw', function () {
                $('input[name="video_checkbox"]').each(function () {
                    this.checked = false;
                });
                $('input[name="main_checkbox"]').prop('checked', false);
                $('button#deleteAllBtn').addClass('d-none');
            });
        })

        // Checkbox
        $(document).on('click', 'input[name="main_checkbox"]', function () {
            if (this.checked) {
                $('input[name="video_checkbox"]').each(function () {
                    this.checked = true;
                });
            } else {
                $('input[name="video_checkbox"]').each(function () {
                    this.checked = false;
                });
            }
            toggledeleteAllBtn();
        });

        $(document).on('change', 'input[name="video_checkbox"]', function () {

            if ($('input[name="video_checkbox"]').length == $('input[name="video_checkbox"]:checked')
                .length) {
                $('input[name="main_checkbox"]').prop('checked', true);
            } else {
                $('input[name="main_checkbox"]').prop('checked', false);
            }
            toggledeleteAllBtn();
        });


        function toggledeleteAllBtn() {
            if ($('input[name="video_checkbox"]:checked').length > 0) {
                $('button#deleteAllBtn').text('Delete (' + $('input[name="video_checkbox"]:checked').length + ')')
                    .removeClass('d-none');
            } else {
                $('button#deleteAllBtn').addClass('d-none');
            }
        }

        $(document).on('click', 'button#deleteAllBtn', function () {
            var checkedData = [];
            $('input[name="video_checkbox"]:checked').each(function () {
                checkedData.push($(this).data('id'));
            });
            var url = '{{ route("video.delete_selected") }}';
            if (checkedData.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    html: 'Apakah anda yakin akan mengpaus <b>(' + checkedData.length + ')</b> Video',
                    showCancelButton: true,
                    showCloseButton: true,
                    confirmButtonText: 'Yes, Delete',
                    cancelButtonText: 'Cancel',
                    confirmButtonColor: '#556ee6',
                    cancelButtonColor: '#d33',
                    width: 300,
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        $.post(url, {
                            video_ids: checkedData
                        }, function (data) {
                            $('#tableVideo').DataTable().ajax.reload(null, true);
                            $.toast({
                                heading: 'Success',
                                position: 'top-right',
                                text: data.message,
                                icon: 'info',
                                loader: true,
                                loaderBg: '#9EC600'
                            })
                        }, 'json');
                    }
                })
            }
        });

        function showForm(data){
            if(data != null && data != undefined) {
                $('input[name="id"]').val(data.id);
                $('input[name="judul"]').val(data.judul);
                $('input[name="video_url"]').val(data.video_url);
                $('textarea[name="description"]').html(data.description);
            }
            $('#inputModal').modal('show');
        }

        function showModal(id, name){
            let url = "{{ url('admin/video') }}/"+id
            $('#slot').html(name)
            $('.btn-modal-delete').attr('href', url);
            $('#notifModal').modal('show')
        }
    </script>
@endpush
