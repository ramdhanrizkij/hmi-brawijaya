<div class="modal fade editMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('menu.save')}}" method="POST" autocomplete="off" id="update-menu-form">
                    {{csrf_field()}}
                    <input type="hidden" name="id" />
                    <div class="form-group">
                        <input type="text" name="nama_menu" id="nama_menu" class="form-control"
                               placeholder="Silahkan masukan Nama Menu">
                        <span class="text-danger error-text menu_error"></span>
                    </div>
                    <div class="form-group">
                        <input type="text" name="link_url" class="form-control"
                               placeholder="Silahkan isi link menu" />
                        <span class="text-danger error-text link_error"></span>
                    </div>
                    <div class="form-group">
                        <input type="text" name="posisi" class="form-control"
                               placeholder="Silahkan isi posisi menu" />
                        <span class="text-danger error-text link_error"></span>
                    </div>
                    <div class="form-group">
                        <select name="parent_id" class="form-control">
                            <option value="" disabled selected hidden>- Pilih Menu Utama -</option>
                            @foreach($allMenus as $key => $row)
                                <option value="{{ $key }}">{{ $row }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary" type="submit"><i
                                class="icon ni ni-save"></i>&nbsp;Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
