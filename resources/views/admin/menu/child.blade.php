<ul>
    @foreach($childs as $row)
        <li>
            {{ $row->nama_menu }}
            @if(count($row->childs))
                @include('admin.menu.child', ['childs' => $row->childs])
            @endif
        </li>
    @endforeach
</ul>
