@extends('layouts.admin')
@section('content')
    <!-- content @s -->
    <div class="nk-content ">
        <div class="container-fluid">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <!-- Head -->
                    <div class="nk-block-head nk-block-head-sm">
                        <div class="nk-block-between">
                            <div class="nk-block-head-content">
                                <h3 class="nk-block-title page-title">Managemen Menu</h3>
                                <div class="nk-block-des text-soft">
                                    <p>Manage Menu</p>
                                </div>
                            </div><!-- .nk-block-head-content -->
                            <div class="nk-block-head-content">
                                <div class="toggle-wrap nk-block-tools-toggle">
                                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"
                                       data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                    <div class="toggle-expand-content" data-content="pageMenu">
                                        <ul class="nk-block-tools g-3">
                                           <!-- <li><a href="#" class="btn btn-white btn-dim btn-outline-primary"><em
                                                        class="icon ni ni-download-cloud"></em><span>Export</span></a></li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .nk-block-head-content -->
                        </div><!-- .nk-block-between -->
                    </div>

                    <!-- Head -->

                    <div class="nk-block">
                        <div class="row g-gs">
                            <div class="col-lg-4">
                                <div class="card card-bordered">
                                    <div class="card-inner">
                                        <div class="card-title">
                                            <h6 class="title">New Menu</h6>
                                        </div>
                                        <div>
                                            <form action="{{route('menu.save')}}" method="POST" autocomplete="off"
                                                  id="add-menu-form">
                                                {{csrf_field()}}
                                                <div class="form-group">
                                                    <input type="text" name="nama_menu" id="nama_menu" class="form-control"
                                                           placeholder="Silahkan masukan Nama Menu">
                                                    <span class="text-danger error-text menu_error"></span>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="link_url" class="form-control"
                                                           placeholder="Silahkan isi link menu" />
                                                    <span class="text-danger error-text link_error"></span>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="posisi" class="form-control"
                                                           placeholder="Silahkan isi posisi menu" />
                                                    <span class="text-danger error-text link_error"></span>
                                                </div>
                                                <div class="form-group">
                                                    <select name="parent_id" class="form-control">
                                                        <option value="" disabled selected hidden>- Pilih Menu Utama -</option>
                                                        @foreach($allMenus as $key => $row)
                                                            <option value="{{ $key }}">{{ $row }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit"><i
                                                            class="icon ni ni-plus"></i>&nbsp;Create</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="card card-bordered">
                                    <div class="card-inner table-responsive">
                                        <h5 class="text-center mb-4 bg-info text-white">Menu</h5>
                                        <ul id="tree1">
                                            @foreach($menus as $row)
                                                <li>
                                                    {{ $row->nama_menu }}
                                                    @if(count($row->childs))
                                                        @include('admin.menu.child', ['childs' => $row->childs])
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div> -->
                            </div>
                            <div class="col-lg-8">
                                <div class="card card-bordered">
                                    <div class="card-inner table-responsive">
                                        <table class="table table-bordered yajra-datatable" id="tableMenu">
                                            <thead>
                                            <tr>
                                                <th width="5%">
                                                    <input type="checkbox" name="main_checkbox"><label></label>
                                                </th>
                                                <th width="8%">No</th>
                                                <th>Nama Menu</th>
                                                <th>Link (URL)</th>
                                                <th>Posisi</th>
                                                <th>Action &nbsp;<button class="btn btn-sm btn-danger d-none"
                                                                         id="deleteAllBtn">Delete All</button></th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.menu.edit')
@endsection

@push('style')
    <link href="{{ asset('css/tree.css') }}" rel="stylesheet"/>
    <style>
        ul.pagination {
            float: right !important;
        }

    </style>
@endpush
@push('script')
    <script src="{{ asset('js/treeview.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Insert
            $('#add-menu-form').on('submit', function (e) {
                e.preventDefault();
                var form = this;
                $.ajax({
                    url: $(form).attr('action'),
                    method: $(form).attr('method'),
                    data: new FormData(form),
                    processData: false,
                    dataType: 'json',
                    contentType: false,
                    beforeSend: function () {
                        $(form).find('span.error-text').text('');
                    },
                    success: function (data) {
                        console.log(data);
                        $(form)[0].reset();
                        $('#tableMenu').DataTable().ajax.reload(null, false);
                        $.toast({
                            heading: 'Success',
                            position: 'top-right',
                            text: data.message,
                            icon: 'info',
                            loader: true,
                            loaderBg: '#9EC600'
                        })
                    },
                    error: function (xhr, status, error) {
                        var err = xhr.responseJSON.errors
                        $.each(err, function (prefix, val) {
                            $(form).find('span.' + prefix + '_error').text(val[0]);
                        })
                    }
                });
            });

            // Update
            $(document).on('click', '#updateMenu', function () {
                var menu_id = $(this).data('id');
                $('.editMenu').find('form')[0].reset();
                $('.editMenu').find('span.error-text').text('');
                $.get(`{{url('admin/menu/detail')}}/${menu_id}`, function (data) {
                    console.log(data);
                    //  alert(data.details.country_name);
                    $('.editMenu').find('input[name="id"]').val(data.id);
                    $('.editMenu').find('input[name="nama_menu"]').val(data.nama_menu);
                    $('.editMenu').find('input[name="link_url"]').val(data.link_url);
                    $('.editMenu').find('input[name="posisi"]').val(data.posisi);
                    $('select[name="parent_id"]').val(data.parent_id).change()

                    $('.editMenu').modal('show');
                }, 'json');
            });

            //UPDATE COUNTRY DETAILS
            $('#update-menu-form').on('submit', function (e) {
                e.preventDefault();
                var form = this;
                $.ajax({
                    url: $(form).attr('action'),
                    method: $(form).attr('method'),
                    data: new FormData(form),
                    processData: false,
                    dataType: 'json',
                    contentType: false,
                    beforeSend: function () {
                        $(form).find('span.error-text').text('');
                    },
                    success: function (data) {
                        $('.editMenu').modal('hide');
                        $(form)[0].reset();
                        $('#tableMenu').DataTable().ajax.reload(null, false);
                        $.toast({
                            heading: 'Success',
                            position: 'top-right',
                            text: data.message,
                            icon: 'info',
                            loader: true,
                            loaderBg: '#9EC600'
                        })
                    },
                    error: function (xhr, status, error) {
                        $('.editMenu').modal('hide');
                        var err = xhr.responseJSON.errors
                        $.each(err, function (prefix, val) {
                            $(form).find('span.' + prefix + '_error').text(val[0]);
                        })
                    }
                });
            });

            // Delete
            $(document).on('click', '#deleteMenu', function () {
                var id = $(this).data('id');
                let fd = new FormData()
                fd.append('id', id)
                var url = '<?= route("menu.destroy") ?>';
                swal.fire({
                    title: 'Are you sure?',
                    html: 'Apakah anda yakin akan <b>menghapus</b> Menu tersebut',
                    showCancelButton: true,
                    showCloseButton: true,
                    cancelButtonText: 'Cancel',
                    confirmButtonText: 'Yes, Delete',
                    cancelButtonColor: '#d33',
                    confirmButtonColor: '#556ee6',
                    width: 300,
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: url,
                            method: 'POST',
                            data: fd,
                            processData: false,
                            dataType: 'json',
                            contentType: false,
                            success: function (data) {
                                $('#tableMenu').DataTable().ajax.reload(null,
                                    false);
                                $.toast({
                                    heading: 'Success',
                                    position: 'top-right',
                                    text: data.message,
                                    icon: 'info',
                                    loader: true,
                                    loaderBg: '#9EC600'
                                })
                            },
                            error: function (xhr, status, error) {
                                $.toast({
                                    heading: 'Error',
                                    text: "Gagal menghapus data menu, terjadi kesalahan dengan server",
                                    icon: 'error',
                                    position: 'top-right'
                                })
                            }
                        });
                    }
                });
            });


            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('menu.table') }}",
                columns: [
                    {
                        data: 'checkbox',
                        name: 'checkbox',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama_menu',
                        name: 'nama_menu'
                    },
                    {
                        data: 'link_url',
                        name: 'link_url'
                    },
                    {
                        data: 'posisi',
                        name: 'posisi'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true,
                        width: '11%'
                    },
                ]
            }).on('draw', function () {
                $('input[name="menu_checkbox"]').each(function () {
                    this.checked = false;
                });
                $('input[name="main_checkbox"]').prop('checked', false);
                $('button#deleteAllBtn').addClass('d-none');
            });
        });

        // Checkbox
        $(document).on('click', 'input[name="main_checkbox"]', function () {
            if (this.checked) {
                $('input[name="menu_checkbox"]').each(function () {
                    this.checked = true;
                });
            } else {
                $('input[name="menu_checkbox"]').each(function () {
                    this.checked = false;
                });
            }
            toggledeleteAllBtn();
        });

        $(document).on('change', 'input[name="menu_checkbox"]', function () {

            if ($('input[name="menu_checkbox"]').length == $('input[name="menu_checkbox"]:checked').length) {
                $('input[name="main_checkbox"]').prop('checked', true);
            } else {
                $('input[name="main_checkbox"]').prop('checked', false);
            }
            toggledeleteAllBtn();
        });


        function toggledeleteAllBtn() {
            if ($('input[name="menu_checkbox"]:checked').length > 0) {
                $('button#deleteAllBtn').text('Delete (' + $('input[name="menu_checkbox"]:checked').length + ')')
                    .removeClass('d-none');
            } else {
                $('button#deleteAllBtn').addClass('d-none');
            }
        }

        $(document).on('click', 'button#deleteAllBtn', function () {
            var checkedData = [];
            $('input[name="menu_checkbox"]:checked').each(function () {
                checkedData.push($(this).data('id'));
            });
            var url = '{{ route("menu.delete_selected") }}';
            if (checkedData.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    html: 'Apakah anda yakin akan mengpaus <b>(' + checkedData.length + ')</b> Menu',
                    showCancelButton: true,
                    showCloseButton: true,
                    confirmButtonText: 'Yes, Delete',
                    cancelButtonText: 'Cancel',
                    confirmButtonColor: '#556ee6',
                    cancelButtonColor: '#d33',
                    width: 300,
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        $.post(url, {
                            menu_ids: checkedData
                        }, function (data) {
                            $('#tableMenu').DataTable().ajax.reload(null, true);
                            $.toast({
                                heading: 'Success',
                                position: 'top-right',
                                text: data.message,
                                icon: 'info',
                                loader: true,
                                loaderBg: '#9EC600'
                            })
                        }, 'json');
                    }
                })
            }
        });

    </script>
@endpush
