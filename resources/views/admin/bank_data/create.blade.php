@extends('layouts.admin')
@push('style')
<style>
    .upload-msg:hover {
        cursor: pointer
    }

    .upload-msg {
        padding-top: 80px;
        text-align: center;
        font-size: 22px;
        color: #aaa;
        margin: 10px auto;
        border: 1px solid #aaa;
    }

    .upload-photo.ready #display {
        display: block;
    }

    .upload-photo.ready .buttons #reset {
        display: inline;
    }

    .upload-photo #display,
    .upload-photo .buttons #reset,
    .upload-photo.ready .upload-msg {
        display: none;
    }

    .hide {
        display: none;
    }

</style>
@endpush
@section('content')
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Managemen Bank Data</h3>
                            <div class="nk-block-des text-soft">
                                <p>Buat Data Baru</p>
                            </div>
                        </div><!-- .nk-block-head-content -->
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"
                                    data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                <div class="toggle-expand-content" data-content="pageMenu">
                                    <ul class="nk-block-tools g-3">
                                        <li><a href="{{url('admin/bank-data')}}"
                                                class="btn btn-white btn-dim btn-outline-primary"><em
                                                    class="icon ni ni-back"></em><span>Kembali</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>

                <div class="nk-block">
                    <form method="post" action="{{route('bank_data.store')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-12">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="row mt-2">

                            <div class="col-md-8">
                                <div class="card card-bordered">
                                    <div class="card-inner">
                                        <div class="form-group">
                                            <label for=""><b>Judul Data</b></label>
                                            <input type="text" name="judul_data"
                                                class="form-control @error('judul_data') is-invalid @enderror"
                                                value="{{old('judul_data')}}">
                                            @error('judul_data')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for=""><b>Deskripsi</b></label>
                                            <textarea name="deskripsi" id="summernote" cols="30" rows="10"
                                                class="form-control @error('deskripsi') is-invalid @enderror">{{old('deskripsi')}}</textarea>
                                            @error('deskripsi')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-bordered">
                                    <div class="card-inner">
                                        <div class="form-group">
                                            <label for=""><b>Kategori</b></label>
                                            <select name="kategori"
                                                class="form-control @error('kategori') is-invalid @enderror">
                                                <option value="">Pilih Kategori</option>
                                                @foreach($kategori as $item)
                                                @if(old('kategori')==$item->id)
                                                <option value="{{$item->id}}" selected>{{$item->kategori}}
                                                </option>
                                                @else
                                                <option value="{{$item->id}}">{{$item->kategori}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            @error('kategori')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for=""><b>Penulis</b></label>
                                            <input type="text" class="form-control" name="penulis" value="{{old('penulis')}}">
                                        </div>

                                        <div class="form-group">
                                            <label for=""><b>Penerbit</b></label>
                                            <input type="text" class="form-control" name="penerbit" value="{{old('penerbit')}}">
                                        </div>

                                        <div class="form-group">
                                            <label for=""><b>Tahun Terbit</b></label>
                                            <input type="text" class="form-control" name="tahun_terbit" value="{{old('tahun_terbit')}}">
                                        </div>

                                        <div class="form-group">
                                            <label for=""><b>File</b></label>
                                            <input type="file" class="form-control" name="file" required>
                                        </div>
                                        <input type="submit" class="btn btn-primary" name="submit" value="Submit">
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
@endsection
@push('script')
<script>
    $(function () {
        $('#summernote').summernote({
            placeholder: 'Ketikkan konten berita anda disini',
            height: 300
        });


        $("#display").hide();
        $(".upload-msg").click(function () {
            $("#upload").click();
        })

        $("#upload").on('change', function (event) {
            if (event.target.files.length > 0) {
                var src = URL.createObjectURL(event.target.files[0]);
                var preview = document.getElementById("image_preview_container");
                preview.src = src;
                preview.style.display = "block";
                $(".upload-msg").hide();
                $("#display").show();
                $(".buttons").show();
            }
        })

        $("#reset").click(function () {
            var preview = document.getElementById("image_preview_container");
            preview.src = "";
            $("#display").hide();
            $(".buttons").hide();
            $(".upload-msg").show();
            $("#upload").val(null);
        });
    })

</script>
@endpush
