@extends('layouts.admin')
@section('content')
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <!-- Head -->
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Bank Data HMI</h3>
                            <div class="nk-block-des text-soft">
                                <p>Daftar Bank Data HMI</p>
                            </div>
                        </div><!-- .nk-block-head-content -->
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"
                                    data-target="pageMenu"><em class="icon ni ni-plus"></em></a>
                                <div class="toggle-expand-content" data-content="pageMenu">
                                    <ul class="nk-block-tools g-3">
                                        <li><a href="{{url('admin/bank-data/create')}}"
                                                class="btn btn-white btn-dim btn-outline-primary"><em
                                                    class="icon ni ni-plus"></em><span>Tambah Data</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>

                <!-- Head -->

                <div class="nk-block">
                    <div class="row g-gs">
                        <div class="col-lg-12">
                            <div class="card card-bordered h-100">
                                <div class="card-inner table-responsive">
                                    <table class="table table-bordered datatable"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Judul</th>
                                                <th>Kategori</th>
                                                <th>Penulis</th>
                                                <th>Penerbit</th>
                                                <th>Tahun Terbit</th>
                                                <th>Tanggal Upload</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($datas as $key=>$item)
                                            <tr>
                                                <td width="5%">{{$key+1}}</td>
                                                <td>{{$item->judul_data}}</td>
                                                <td width="20%">{{$item->kategori?$item->kategori->kategori:'-'}}</td>
                                                <td>{{$item->penulis ?? '-'}}</td>
                                                <td>{{$item->penerbit ?? '-'}}</td>
                                                <td>{{$item->tahun_terbit ?? '-'}}</td>
                                                <td width="10%">{{$item->created_at->format('d M Y H:i:s')}}</td>
                                                <td width="20%">
                                                    <a href="{{url('/admin/bank-data/'.$item->id)}}" class="btn btn-primary">Update</a>
                                                    <a href="{{url('admin/bank-data/delete/'.$item->id)}}" class="btn btn-danger" onClick="return confirm('Apakah anda yakin akan menghapus data product tersebut ?')">Delete</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(document).ready(function(){
        $(".datatable").DataTable();
    })
    </script>
   
@endpush
@push('style')
<style>
    ul.pagination {
        float: right !important;
    }

</style>
@endpush
