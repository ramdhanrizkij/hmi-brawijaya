@extends('layouts.admin')
@section('content')
    <!-- content @s -->
    <div class="nk-content ">
        <div class="container-fluid">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <!-- Head -->
                    <div class="nk-block-head nk-block-head-sm">
                        <div class="nk-block-between">
                            <div class="nk-block-head-content">
                                <h3 class="nk-block-title page-title">Managemen Role</h3>
                                <div class="nk-block-des text-soft">
                                    <p>Manage Role</p>
                                </div>
                            </div><!-- .nk-block-head-content -->
                            <div class="nk-block-head-content">
                                <div class="toggle-wrap nk-block-tools-toggle">
                                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"
                                       data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                    <div class="toggle-expand-content" data-content="pageMenu">
                                        <ul class="nk-block-tools g-3">
                                           <!-- <li><a href="#" class="btn btn-white btn-dim btn-outline-primary"><em
                                                        class="icon ni ni-download-cloud"></em><span>Export</span></a></li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .nk-block-head-content -->
                        </div><!-- .nk-block-between -->
                    </div>

                    <!-- Head -->

                    <div class="nk-block">
                        <div class="row g-gs">
                            <div class="col-lg-4">
                                <div class="card card-bordered">
                                    <div class="card-inner">
                                        <div class="card-title">
                                            <h6 class="title">New Role</h6>
                                        </div>
                                        <div>
                                            <form action="{{route('role.save')}}" method="POST" autocomplete="off"
                                                  id="add-roles-form">
                                                {{csrf_field()}}
                                                <div class="form-group">
                                                    <input type="text" name="role_name" id="role_name" class="form-control"
                                                           placeholder="Please type role name">
                                                    <span class="text-danger error-text role_error"></span>
                                                </div>

                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit"><i
                                                            class="icon ni ni-plus"></i>&nbsp;Create</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner table-responsive">

                                        <table class="table table-bordered yajra-datatable" id="tableRole">
                                            <thead>
                                            <tr>
                                                <th width="5%"><input type="checkbox"
                                                                      name="main_checkbox"><label></label></th>
                                                <th width="8%">No</th>
                                                <th>Nama Role</th>
                                                <th>Action &nbsp;<button class="btn btn-sm btn-danger d-none"
                                                                         id="deleteAllBtn">Delete All</button></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.role.edit')
@endsection

@push('style')
    <style>
        ul.pagination {
            float: right !important;
        }

    </style>
@endpush
@push('script')
    <script type="text/javascript">
        $(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Insert
            $('#add-roles-form').on('submit', function (e) {
                e.preventDefault();
                var form = this;
                $.ajax({
                    url: $(form).attr('action'),
                    method: $(form).attr('method'),
                    data: new FormData(form),
                    processData: false,
                    dataType: 'json',
                    contentType: false,
                    beforeSend: function () {
                        $(form).find('span.error-text').text('');
                    },
                    success: function (data) {
                        console.log(data);
                        $(form)[0].reset();
                        $('#tableRole').DataTable().ajax.reload(null, false);
                        $.toast({
                            heading: 'Success',
                            position: 'top-right',
                            text: data.message,
                            icon: 'info',
                            loader: true,
                            loaderBg: '#9EC600'
                        })
                    },
                    error: function (xhr, status, error) {
                        var err = xhr.responseJSON.errors
                        $.each(err, function (prefix, val) {
                            $(form).find('span.' + prefix + '_error').text(val[0]);
                        })
                    }
                });
            });

            // Update
            $(document).on('click', '#updateRole', function () {
                var role_id = $(this).data('id');
                $('.editRole').find('form')[0].reset();
                $('.editRole').find('span.error-text').text('');
                $.get(`{{url('admin/role/detail')}}/${role_id}`, function (data) {
                    console.log(data);
                    //  alert(data.details.country_name);
                    $('.editRole').find('input[name="id"]').val(data.id);
                    $('.editRole').find('input[name="role_name"]').val(data.role_name);
                    $('.editRole').modal('show');
                }, 'json');
            });

            //UPDATE COUNTRY DETAILS
            $('#update-role-form').on('submit', function (e) {
                e.preventDefault();
                var form = this;
                $.ajax({
                    url: $(form).attr('action'),
                    method: $(form).attr('method'),
                    data: new FormData(form),
                    processData: false,
                    dataType: 'json',
                    contentType: false,
                    beforeSend: function () {
                        $(form).find('span.error-text').text('');
                    },
                    success: function (data) {
                        $('.editRole').modal('hide');
                        $(form)[0].reset();
                        $('#tableRole').DataTable().ajax.reload(null, false);
                        $.toast({
                            heading: 'Success',
                            position: 'top-right',
                            text: data.message,
                            icon: 'info',
                            loader: true,
                            loaderBg: '#9EC600'
                        })
                    },
                    error: function (xhr, status, error) {
                        $('.editRole').modal('hide');
                        var err = xhr.responseJSON.errors
                        $.each(err, function (prefix, val) {
                            $(form).find('span.' + prefix + '_error').text(val[0]);
                        })
                    }
                });
            });

            // Delete
            $(document).on('click', '#deleteRole', function () {
                var id = $(this).data('id');
                let fd = new FormData()
                fd.append('id', id)
                var url = '<?= route("role.destroy") ?>';
                swal.fire({
                    title: 'Are you sure?',
                    html: 'Apakah anda yakin akan <b>menghapus</b> Role tersebut',
                    showCancelButton: true,
                    showCloseButton: true,
                    cancelButtonText: 'Cancel',
                    confirmButtonText: 'Yes, Delete',
                    cancelButtonColor: '#d33',
                    confirmButtonColor: '#556ee6',
                    width: 300,
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: url,
                            method: 'POST',
                            data: fd,
                            processData: false,
                            dataType: 'json',
                            contentType: false,
                            success: function (data) {
                                $('#tableRole').DataTable().ajax.reload(null,
                                    false);
                                $.toast({
                                    heading: 'Success',
                                    position: 'top-right',
                                    text: data.message,
                                    icon: 'info',
                                    loader: true,
                                    loaderBg: '#9EC600'
                                })
                            },
                            error: function (xhr, status, error) {
                                $.toast({
                                    heading: 'Error',
                                    text: "Gagal menghapus data role, terjadi kesalahan dengan server",
                                    icon: 'error',
                                    position: 'top-right'
                                })
                            }
                        });
                    }
                });
            });


            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('role.table') }}",
                columns: [{
                    data: 'checkbox',
                    name: 'checkbox',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                    {
                        data: 'role_name',
                        name: 'role_name'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true,
                        width: '11%'
                    },
                ]
            }).on('draw', function () {
                $('input[name="role_checkbox"]').each(function () {
                    this.checked = false;
                });
                $('input[name="main_checkbox"]').prop('checked', false);
                $('button#deleteAllBtn').addClass('d-none');
            });
        });

        // Checkbox
        $(document).on('click', 'input[name="main_checkbox"]', function () {
            if (this.checked) {
                $('input[name="role_checkbox"]').each(function () {
                    this.checked = true;
                });
            } else {
                $('input[name="role_checkbox"]').each(function () {
                    this.checked = false;
                });
            }
            toggledeleteAllBtn();
        });

        $(document).on('change', 'input[name="role_checkbox"]', function () {

            if ($('input[name="role_checkbox"]').length == $('input[name="role_checkbox"]:checked')
                .length) {
                $('input[name="main_checkbox"]').prop('checked', true);
            } else {
                $('input[name="main_checkbox"]').prop('checked', false);
            }
            toggledeleteAllBtn();
        });


        function toggledeleteAllBtn() {
            if ($('input[name="role_checkbox"]:checked').length > 0) {
                $('button#deleteAllBtn').text('Delete (' + $('input[name="role_checkbox"]:checked').length + ')')
                    .removeClass('d-none');
            } else {
                $('button#deleteAllBtn').addClass('d-none');
            }
        }

        $(document).on('click', 'button#deleteAllBtn', function () {
            var checkedData = [];
            $('input[name="role_checkbox"]:checked').each(function () {
                checkedData.push($(this).data('id'));
            });
            var url = '{{ route("role.delete_selected") }}';
            if (checkedData.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    html: 'Apakah anda yakin akan mengpaus <b>(' + checkedData.length + ')</b> Role',
                    showCancelButton: true,
                    showCloseButton: true,
                    confirmButtonText: 'Yes, Delete',
                    cancelButtonText: 'Cancel',
                    confirmButtonColor: '#556ee6',
                    cancelButtonColor: '#d33',
                    width: 300,
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        $.post(url, {
                            role_ids: checkedData
                        }, function (data) {
                            $('#tableRole').DataTable().ajax.reload(null, true);
                            $.toast({
                                heading: 'Success',
                                position: 'top-right',
                                text: data.message,
                                icon: 'info',
                                loader: true,
                                loaderBg: '#9EC600'
                            })
                        }, 'json');
                    }
                })
            }
        });

    </script>
@endpush
