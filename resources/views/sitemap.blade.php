@extends('layouts.app')
@section('content')
    <div class="container space-20 space-padding-tb-20">
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li class="active">Sitemap</li>
        </ul>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="single-post">

                    <div class="blog-post-item cat-1 box">
                        <div class="content">
                            <p><a href="{{ url('/') }}" target="_open">MainRugby.com</a></p>
                            <p><a href="{{ url('/kontak') }}" target="_open"> Kontak MainRugby.com</a></p>
                            <p><a href="{{ url('/syarat-dan-ketentuan') }}" target="_open">Syarat dan Kententuan MainRugby.com</a></p>
                            <p><a href="{{ url('/tentang') }}" target="_open">Tentang MainRugby.com</a></p>
                            @foreach($datas as $key => $values)
                                    <h3>{{ $key }}</h3>
{{--                                <div class="title-v1 box">--}}
{{--                                </div>--}}
                                @foreach($values as $row)
                                    <p><a href="{{ url('/berita')."/".$row->slug }}" target="_open">{{ "- ".$row->judul }}</a></p>
                                @endforeach
                            @endforeach
                            <p><a href="{{ url('/toko') }}" target="_open">Toko</a></p>
                            <br>
                        </div>
                        <!-- End slider -->
                        <!-- End title -->
                        <!-- End title -->
                        <!-- End box -->
                    </div>
                </div>
                <!-- End single-post -->
            </div>
        </div>
    </div>
    <!-- End container -->

    <!-- End box-bottom -->
    <div id="back-to-top">
        <i class="fa fa-long-arrow-up"></i>
    </div>
@endsection
