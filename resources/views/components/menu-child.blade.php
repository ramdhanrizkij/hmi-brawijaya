<ul class="menu-level-1 list-menu">
    @foreach($childs as $row)
        <li class="level2"><a href="{{ $row->link_url }}" title="{{ $row->nama_menu }}">{{ $row->nama_menu }}</a></li>
        @if(count($row->childs))
            @include('components.menu-child', ['childs' => $row->childs])
        @endif
    @endforeach
</ul>
