<div class="tp-banner-container hidden-dot hidden-nav ver1 space_70 hidden-desktop">
    <div class="tp-banner">
        <ul>
            @foreach($banner as $item)
            <!-- SLIDE  -->
            <li data-transition="random" data-slotamount="6" data-masterspeed="1000" data-saveperformance="on"
                data-title="{{$item->judul}}">
                <!-- MAIN IMAGE -->
                <img src="{{asset('storage/banner/'.$item->banner_img)}}" alt="Futurelife-home2-slideshow"
                    data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                <div class="tp-caption title_slider" data-x="388" data-y="290"
                    data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                    data-speed="800" data-start="1600" data-easing="Power4.easeOut" data-endspeed="300"
                    data-endeasing="Power1.easeIn" data-captionhidden="on" style="z-index: 9"><a href="{{$item->link}}" title="title"
                        class="title border">{{$item->judul}}</a>
                </div>
            </li>
            @endforeach
            <!-- SLIDER -->
        </ul>
        <div class="tp-bannertimer"></div>
    </div>
</div>
<!-- End tp-banner -->
<div class="home2-head box slide-owl-mobile slide-mobile-home1 nav-ver5">
    <div class="col-md-4 col-sm-4">
        <div class="post-item ver1 cat-1 overlay">
            @if($homepage1 && $homepage1->banner_image)
            <a class="images" href="{{$homepage1->link_url}}" title="images"><img class='img-responsive'
                    src="{{asset('uploads/banner_iklan/'.$homepage1->banner_image)}}" alt="images" style="height:151px;object-fit:cover;width:100%;"></a>
            @else
            <a class="images" href="#" title="images"><img class='img-responsive'
                    src="{{asset('frontend/images/home1-slideshow1.jpg')}}" alt="images"></a>
            @endif
        </div>
        <!-- End item -->
    </div>
    <!-- End col-md-4 -->
    <div class="col-md-4 col-sm-4">
        <div class="post-item ver1 cat-2 overlay">

                @if($homepage2 && $homepage2->banner_image)
                <a class="images" href="{{$homepage2->link_url}}" title="images"><img class='img-responsive'
                        src="{{asset('uploads/banner_iklan/'.$homepage2->banner_image)}}" alt="images" style="height:151px;object-fit:cover;width:100%;"></a>
                @else
                <a class="images" href="#" title="images"><img class='img-responsive'
                        src="{{asset('frontend/images/home1-slideshow1.jpg')}}" alt="images"></a>
                @endif
        </div>
        <!-- End item -->
    </div>
    <!-- End col-md-4 -->
    <div class="col-md-4 col-sm-4">
        <div class="post-item ver1 cat-3 overlay">
            @if($homepage3 && $homepage3->banner_image)
            <a class="images" href="{{$homepage3->link_url}}" title="images"><img class='img-responsive'
                    src="{{asset('uploads/banner_iklan/'.$homepage3->banner_image)}}" alt="images" style="height:151px;object-fit:cover;width:100%;"></a>
            @else
            <a class="images" href="#" title="images"><img class='img-responsive'
                    src="{{asset('frontend/images/home1-slideshow1.jpg')}}" alt="images"></a>
            @endif
        </div>
        <!-- End item -->
    </div>
    <!-- End col-md-4 -->
</div>
<!-- End home-head -->
