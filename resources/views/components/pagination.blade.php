@if($paginator->hasPages())
    @if($paginator->onFirstPage())
        <a class="control prev" href="#" title="pre"><i class="fa fa-long-arrow-left"></i>Previous</a>
    @else
        <a class="control prev" href="{{ $paginator->previousPageUrl() }}" title="pre"><i class="fa fa-long-arrow-left"></i>Previous</a>
    @endif

    <ul>
    @foreach($elements as $row)
        @if(is_string($row))
            <li><a href="#">{{ $row }}</a></li>
        @endif

        @if(is_array($row))
            @foreach($row as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li class="active"><a href="#">{{ $page }}</a></li>
                @else
                    <li><a href="{{ $url }}">{{ $page }}</a></li>
                @endif
            @endforeach
        @endif

    @endforeach
    </ul>

    @if($paginator->hasMorePages())
        <a class="control next" href="{{ $paginator->nextPageUrl() }}" title="next">Next<i class="fa fa-long-arrow-right"></i></a>
    @else
        <a class="control next" href="#" title="next">Next<i class="fa fa-long-arrow-right"></i></a>
    @endif
@endif
