<div>
    <dic class="card">
        <div class="card-header bg-primary-dim">
            <h5 class="card-title">{{ $title }}</h5>
        </div>
        <div class="card-body">
            {{ $slot }}
        </div>
        @if($withFooter)
            <div class="card-footer bg-primary-dim">{{ $footer }}</div>
        @endif
    </dic>
</div>
