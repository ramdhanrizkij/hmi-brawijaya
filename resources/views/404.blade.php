@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="page-404 center space-padding-tb-40">
            <h3 class="space-30">404</h3>
            <p><b>Mohom Maaf, Halaman atau Kata Kunci yang anda cari tidak ditemukan.</b></p>
            <p class="space-30">Kembali ke <a href="{{ url('/') }}" title="Homepage">Home<i class="fa fa-angle-double-right"></i></a></p>
        </div>
        <!-- End page-404 -->
    </div>
    <!-- End container -->

    <!-- End box-bottom -->
    <div id="back-to-top">
        <i class="fa fa-long-arrow-up"></i>
    </div>
@endsection
