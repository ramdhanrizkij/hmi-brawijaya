@if ($paginator->hasPages())
<nav class="pagination">
    @if (!$paginator->onFirstPage())
    <a class="control prev" href="{{ $paginator->previousPageUrl()}}" title="pre"><i
            class="fa fa-long-arrow-left"></i>Previous</a>
    @endif
    <ul>
        @foreach ($elements as $element)
        @if (is_string($element))
        <li><a href="#" class="disabled" title="{{$element}}">{{$element}}}</a></li>
        @endif



        @if (is_array($element))
        @foreach ($element as $page => $url)
        @if ($page == $paginator->currentPage())
        <li     >{{ $page }}</li>
        @else
        <li><a href="{{ $url }}">{{ $page }}</a></li>
        @endif
        @endforeach
        @endif
        @endforeach

    </ul>
    @if ($paginator->hasMorePages())
    <a class="control next" href="{{$paginator->nextPageUrl() }}" title="next">Next<i
            class="fa fa-long-arrow-right"></i></a>

    @endif
</nav>
@endif
