@extends('layouts.app')
@section('content')
    <div class="container space-20 space-padding-tb-20">
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li class="active">Kontak</li>
        </ul>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="single-post">
                    <div id="googleMap"></div>
                    <!-- End googlemap -->
                    <div class="blog-post-item cat-1 box">
                        <div class="title-v1">
                            <h3>Hubungi MainRugby.Com</h3>
                        </div>
                        <!-- End title -->
                        <div class="box">
                            <form id="form-contact-us" action="{{ route('contact.save') }}" method="POST" class="form-horizontal space-50">
                                <div class="head">
                                    <div class="form-group col-md-4 space-30">
                                        <label class="control-label" for="inputName">Name *</label>
                                        <input type="text" name="nama" id="inputName" class="form-control">
                                        <span class="text-danger error-text nama_error"></span>
                                    </div>
                                    <div class="form-group col-md-4 space-30">
                                        <label class="control-label" for="inputemail">Email *</label>
                                        <input type="email" name="email" id="inputemail" class="form-control">
                                        <span class="text-danger error-text email_error"></span>
                                    </div>
                                    <div class="form-group col-md-4 space-30">
                                        <label class="control-label" for="inputwebsite">Website</label>
                                        <input type="text" name="website" id="inputwebsite" class="form-control">
                                        <span class="text-danger error-text website_error"></span>
                                    </div>
                                </div>
                                <!-- End head -->
                                <div class="form-group">
                                    <label class="control-label" for="message">Your message *</label>
                                    <textarea name="pesan" id="message" class="form-control"></textarea>
                                    <span class="text-danger error-text pesan_error"></span>
                                </div>
                                <a title="send message" href="#" onclick="$('#form-contact-us').submit()" class="button">Send Message</a>
                            </form>
                        </div>
                        <!-- End box -->
                    </div>
                </div>
                <!-- End single-post -->
            </div>

            @if(sizeof($populer)>0)
                <div class="col-md-4">
                    <aside class="widget popular">
                        <h3 class="widget-title">Terpopuler</h3>
                        <?php $item = $populer[0];?>
                        <div class="content">
                            <div class="post-item ver1 overlay">
                                <a class="images" href="{{url('/berita/'.$item->slug)}}" title="images"><img class='img-responsive'
                                                                                                             src="{{asset('uploads/berita/'.$item->thumbnail)}}" alt="images"></a>
                                <div class="text">
                                    <h2><a href="{{url('berita/'.$item->slug)}}" title="title">{{$item->judul}}</a></h2>
                                    <div class="tag">
                                        <p class="date"><i class="fa fa-clock-o"></i>{{$item->created_at->format('M d,Y')}}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- End item -->
                            @foreach($populer as $key=>$item)
                                @if($key>0)
                                    <div class="post-item ver2">
                                        <a class="images" href="{{url('/berita/'.$item->slug)}}" title="images"><img class='img-responsive'
                                                                                                                     src="{{asset('uploads/berita/'.$item->thumbnail)}}" alt="images"></a>
                                        <div class="text">
                                            <h2><a href="{{url('berita/'.$item->slug)}}" title="title">{{$item->judul}}</a></h2>
                                            <div class="tag">
                                                <p class="date"><i class="fa fa-clock-o"></i>{{$item->created_at->format('M d, Y')}}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </aside>
                    <aside class="widget">
                        <div class="banner">
                            @if($sidebar && $sidebar->banner_image)
                                <img class="img-responsive" src="{{asset('uploads/banner_iklan/'.$sidebar->banner_image)}}" alt="banner">
                            @else
                                <img class="img-responsive" src="{{asset('frontend/images/widget-banner.jpg')}}" alt="banner">
                            @endif

                        </div>
                    </aside>
                </div>
            @endif
        </div>
    </div>
    <!-- End container -->

    <!-- End box-bottom -->
    <div id="back-to-top">
        <i class="fa fa-long-arrow-up"></i>
    </div>
@endsection
@push('script')
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#form-contact-us').on('submit', function (e) {
            e.preventDefault();
            var form = this;
            $.ajax({
                url: $(form).attr('action'),
                method: $(form).attr('method'),
                data: new FormData(form),
                processData: false,
                dataType: 'json',
                contentType: false,
                beforeSend: function () {
                    $(form).find('span.error-text').text('');
                },
                success: function (data) {
                    console.log(data);
                    $(form)[0].reset();
                    $.toast({
                        heading: 'Success',
                        position: 'top-right',
                        text: data.message,
                        icon: 'info',
                        loader: true,
                        loaderBg: '#9EC600'
                    })
                },
                error: function (xhr, status, error) {
                    var err = xhr.responseJSON.errors
                    $.each(err, function (prefix, val) {
                        $(form).find('span.' + prefix + '_error').text(val[0]);
                    })
                }
            });
        });
    </script>
@endpush
