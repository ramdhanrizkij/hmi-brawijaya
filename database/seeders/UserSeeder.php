<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'Administrator',
            'email'=>'admin@hmibrawijaya.com',
            'password'=>bcrypt('admin'),
            'photo_url'=>'noimage.png',
            'role_id'=>1
        ]);

        User::create([
            'name'=>'Member',
            'email'=>'member1@hmibrawijaya.com',
            'password'=>bcrypt('member'),
            'photo_url'=>'noimage.png',
            'role_id'=>2
        ]);
    }
}
