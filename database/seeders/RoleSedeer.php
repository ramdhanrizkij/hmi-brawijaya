<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
class RoleSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'id'=>1,
            'role_name'=>'Administrator'
        ]);

        Role::create([
            'id'=>2,
            'role_name'=>'Member'
        ]);
    }
}
