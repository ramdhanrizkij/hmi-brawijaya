<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\BankDataController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'index']);

Auth::routes();

Route::get('kategori/{slug}',[App\Http\Controllers\BeritaController::class,'newsKategori']);
Route::prefix('berita')->group(function(){
    Route::get('/',[App\Http\Controllers\BeritaController::class,'newsList']);
    Route::get('{slug}',[App\Http\Controllers\BeritaController::class,'readNews']);
});

Route::get('/search', [\App\Http\Controllers\SearchController::class, 'index'])->name('search');
Route::get('/kontak', [\App\Http\Controllers\HomeController::class, 'kontak'])->name('kontak');
Route::get('/sitemap', [\App\Http\Controllers\HomeController::class, 'sitemap'])->name('sitemap');

Route::get('/page/{slug}',[\App\Http\Controllers\PageController::class,'read']);
Route::prefix('admin')->group(function () {
    Route::get('/page/{slug}',[App\Http\Controllers\PageController::class,'update']);
    Route::post('/page/{id}',[App\Http\Controllers\PageController::class,'storeUpdate'])->name('page.update');
    Route::get('/',[App\Http\Controllers\DashboardController::class,'index']);

    Route::get('/profile', [\App\Http\Controllers\UserController::class, 'profile'])->name('profile');
    Route::post('/change-password', [\App\Http\Controllers\UserController::class, 'changePwd'])->name('change.password');

    Route::prefix('role')->group(function (){
        Route::get('/', [\App\Http\Controllers\RoleController::class, 'index'])->name('role.index');
        Route::post('/', [\App\Http\Controllers\RoleController::class, 'save'])->name('role.save');
        Route::post('/delete', [\App\Http\Controllers\RoleController::class, 'destroy'])->name('role.destroy');
        Route::get('/detail/{id}', [\App\Http\Controllers\RoleController::class, 'findById'])->name('role.detail');
        Route::get('/table', [\App\Http\Controllers\RoleController::class, 'tableAPI'])->name('role.table');

        Route::post('/delete-selected', [\App\Http\Controllers\RoleController::class, 'deleteSelected'])->name('role.delete_selected');
    });

    Route::prefix('banner')->group(function(){
        Route::get('/',[BannerController::class,'index'])->name('banner.index');
        Route::post('/update',[BannerController::class,'update'])->name('banner.update');
    });

    Route::prefix('slider')->group(function (){
        Route::get('/', [\App\Http\Controllers\SliderController::class, 'index'])->name('slider.index');
        Route::post('/', [\App\Http\Controllers\SliderController::class, 'save'])->name('slider.save');
        Route::get('/{id}', [\App\Http\Controllers\SliderController::class, 'destroy'])->name('slider.destroy');
    });

    Route::prefix('video')->group(function () {
        Route::get('/', [\App\Http\Controllers\VideoController::class, 'index'])->name('video.index');
        Route::post('/', [\App\Http\Controllers\VideoController::class, 'save'])->name('video.save');
        Route::post('/delete', [\App\Http\Controllers\VideoController::class, 'destroy'])->name('video.destroy');
        Route::get('/table', [\App\Http\Controllers\VideoController::class, 'tableAPI'])->name('video.table');

        Route::post('/delete-selected', [\App\Http\Controllers\VideoController::class, 'deleteSelected'])->name('video.delete_selected');
    });

    Route::prefix('categories')->group(function(){
        Route::get('/',[\App\Http\Controllers\KategoriController::class,'index'])->name('kategori.index');
        Route::get('/table',[\App\Http\Controllers\KategoriController::class,'tableAPI'])->name("kategori.table");
        Route::get('/detail/{id}',[\App\Http\Controllers\KategoriController::class,'getById']);
        Route::post('/',[\App\Http\Controllers\KategoriController::class,'create'])->name("kategori.save");

        Route::post('/delete',[\App\Http\Controllers\KategoriController::class,'delete'])->name('kategori.delete');
        Route::post('/delete-selected',[\App\Http\Controllers\KategoriController::class,'deleteSelected'])->name('kategori.delete_selected');
        Route::post('/update',[\App\Http\Controllers\KategoriController::class,'update'])->name('kategori.update');
    });


    Route::prefix('berita')->group(function(){
        Route::get('/',[\App\Http\Controllers\BeritaController::class,'index'])->name('berita.index');
        Route::get('/create',[\App\Http\Controllers\BeritaController::class,'create'])->name('berita.create');
        Route::post('/',[\App\Http\Controllers\BeritaController::class,'store'])->name('berita.store');
        Route::get('/table',[\App\Http\Controllers\BeritaController::class,'table'])->name('berita.table');
        Route::get('/update/{id}',[\App\Http\Controllers\BeritaController::class,'update']);
        Route::post('/update/{id}',[\App\Http\Controllers\BeritaController::class,'saveUpdate'])->name('berita.update');
        Route::get('/{id}/{action}',[\App\Http\Controllers\BeritaController::class,'action'])->name('berita.action');
        Route::get('/delete/{id}',[\App\Http\Controllers\BeritaController::class,'delete']);
        Route::post('/bulk-delete',[\App\Http\Controllers\BeritaController::class,'bulkDelete'])->name('berita.bulk_delete');
    });

    Route::prefix('product')->group(function(){
        Route::get('/',[ProductController::class,'index'])->name('product.index');
        Route::get('/delete/{id}',[ProductController::class,'delete']);
        Route::get('/create',[ProductController::class,'addProduct']);
        Route::post('/create',[ProductController::class,'storeProduct'])->name('product.store');
        Route::get('/kategori',[ProductController::class,'addKategori'])->name('product_kategori.index');
        Route::post('/kategori',[ProductController::class,'saveKategori'])->name('product_kategori.save');
        Route::get('/kategori/delete/{id}',[ProductController::class,'deleteKategori'])->name('product_kategori.delete');
        Route::post('/kategori/update',[ProductController::class,'updateKategori'])->name('product_kategori.update');
        Route::get('/image-delete/{id}',[ProductController::class,'deleteImage']);
        Route::get('/{id}', [ProductController::class,'updateProduct']);
        Route::post('/{id}', [ProductController::class,'storeUpdate'])->name('product.save');
    });

    Route::prefix('user')->group(function (){
        Route::get('/',[\App\Http\Controllers\UserController::class,'index'])->name('user.index');
        Route::get('/table',[\App\Http\Controllers\UserController::class,'tableAPI'])->name('user.table');
        Route::post('/save',[\App\Http\Controllers\UserController::class,'save'])->name('user.save');
        Route::get('/detail/{id}',[\App\Http\Controllers\UserController::class,'getById']);

        Route::get('/delete',[\App\Http\Controllers\UserController::class,'delete'])->name('user.destroy');
        Route::post('/bulk-delete',[\App\Http\Controllers\UserController::class,'deleteSelected'])->name('user.delete_selected');
    });

    Route::prefix('menu')->group(function () {
        Route::get('/', [\App\Http\Controllers\MenuController::class, 'index'])->name('menu.index');
        Route::get('/show', [\App\Http\Controllers\MenuController::class, 'show'])->name('menu.show');
        Route::post('/', [\App\Http\Controllers\MenuController::class, 'save'])->name('menu.save');
        Route::get('/table', [\App\Http\Controllers\MenuController::class, 'tableAPI'])->name('menu.table');
        Route::post('/delete', [\App\Http\Controllers\MenuController::class, 'destroy'])->name('menu.destroy');
        Route::get('/detail/{id}', [\App\Http\Controllers\MenuController::class, 'findById'])->name('menu.detail');

        Route::post('/delete-selected',[\App\Http\Controllers\MenuController::class, 'deleteSelected'])->name('menu.delete_selected');
    });

    Route::prefix('bank-data')->group(function(){
        Route::get('/',[BankDataController::class,'index'])->name('bank_data.index');
        Route::get('/create',[BankDataController::class,'create'])->name('bank_data.create');
        Route::post('/create',[BankDataController::class,'store'])->name('bank_data.store');
        Route::get("{id}",[BankDataController::class,'update'])->name('bank_data.update');
        Route::post("{id}",[BankDataController::class,'save'])->name('bank_data.save');
        Route::get('/delete/{id}',[BankDataController::class,'delete']);
    });

    Route::prefix('gallery')->group(function (){
        Route::get('/', [\App\Http\Controllers\GalleryController::class, 'index'])->name('gallery.index');
        Route::post('/', [\App\Http\Controllers\GalleryController::class, 'save'])->name('gallery.save');
        Route::get('/delete/{id}', [\App\Http\Controllers\GalleryController::class, 'destroy'])->name('gallery.destroy');
    });

    Route::prefix('contact')->group(function () {
        Route::get('/', [\App\Http\Controllers\ContactController::class, 'index'])->name('contact.index');
        Route::post('/', [\App\Http\Controllers\ContactController::class, 'save'])->name('contact.save');
        Route::get('/table', [\App\Http\Controllers\ContactController::class, 'tableAPI'])->name('contact.table');
        Route::post('/delete', [\App\Http\Controllers\ContactController::class, 'destroy'])->name('contact.destroy');

        Route::post('/delete-selected', [\App\Http\Controllers\ContactController::class, 'deleteSelected'])->name('contact.delete_selected');
    });
});

